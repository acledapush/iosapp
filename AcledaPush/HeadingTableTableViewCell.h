//
//  HeadingTableTableViewCell.h
//  AcledaPush
//
//  Created by Abhishek Ingle on 27/04/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeadingTableTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *userIdCellDetails;

@end
