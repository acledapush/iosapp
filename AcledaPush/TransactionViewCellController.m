//
//  TransactionViewCellController.m
//  AcledaPush
//
//  Created by Abhishek Ingle on 23/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import "TransactionViewCellController.h"
#import "TransactionDetails.h"
#import "Details.h"
#import "SuccessViewController.h"
#import "ErrorViewController.h"
#import "SWRevealViewController.h"

@interface TransactionViewCellController ()<UITableViewDataSource, UITableViewDelegate>{
    NSMutableArray *pushTransactionDetails;
}
@property (weak, nonatomic) IBOutlet UITableView *txTable;


@end

@implementation TransactionViewCellController
NSString *txId;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self arraySetUp];    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)arraySetUp {
    pushTransactionDetails = [NSMutableArray arrayWithArray:@[@"Action",@"IP",@"Location",@"Date",@"Expiry Time",@"Message"]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return pushTransactionDetails.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"tCell";
    TransactionDetails *txObj = [self returnPushData];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    cell.textLabel.text = pushTransactionDetails[indexPath.row];
    int txRow = (int)indexPath.row + 1;
    txId=txObj.transactionID;
    
    if(txRow == 1){
        if([txObj.transactionAction isEqualToString:@"T"]){
            cell.detailTextLabel.text = @"Transaction";
        }else if([txObj.transactionAction isEqualToString:@"L"]){
            cell.detailTextLabel.text = @"Login";
        }else{
            cell.detailTextLabel.text = @"NA";
        }
    }
    else if(txRow == 2){
        cell.detailTextLabel.text = txObj.transactionIp;
    }else if(txRow == 3){
        cell.detailTextLabel.text = txObj.transactionLocation;
    }else if(txRow == 4){
        cell.detailTextLabel.text = txObj.transactionDate;
    }else if(txRow == 5){
        cell.detailTextLabel.text = txObj.transactionExpiryTime;
    }else if(txRow == 6){
        cell.detailTextLabel.text = txObj.transactionMessage;
    }
//    else if(txRow == 7){
//        if(txObj.transactionStatus == 0){
//            cell.detailTextLabel.text = @"Pending";
//        }else if([txObj.transactionStatus isEqualToString:@"1"]){
//            cell.detailTextLabel.text = @"Approved";
//        }else if([txObj.transactionStatus isEqualToString:@"2"]){
//            cell.detailTextLabel.text = @"Denied";
//        }
//    }
    
    return cell;
}
-(NSString *) LoadFile:(NSString *)filename{
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *strFileName = [NSString stringWithFormat:@"%@/%@",
                             documentsDirectory,filename];
    NSString *content = [[NSString alloc] initWithContentsOfFile:strFileName
                                                    usedEncoding:nil
                                                           error:nil];
    return content;
    //use simple alert from my library (see previous post for details)
    // [ASFunctions alert:content];
    //  [content release];
    
}

-(TransactionDetails *) returnPushData{
    NSString* txIP = @"NA";
    NSString* txAction = @"NA";
    NSString* txMessage = @"NA";
    NSString* txDate = @"NA";
    NSString* txExpiry = @"NA";
    NSString* txID = @"NA";
    //NSString* txStatus = @"9";
    NSString* txLocation = @"NA";
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *currentUserLogin = [prefs objectForKey:@"currentLogiedInUser"];
    
    NSLog(@"user returnPushData >> %@",currentUserLogin);
    TransactionDetails *transactionDetails = [[TransactionDetails alloc]init];
    NSString *strPreviousJsonEncData = [self LoadFile:[@"livePushDetails" stringByAppendingString:currentUserLogin]];
    NSLog(@"TxFileReadDate %@",strPreviousJsonEncData);
    
    if(strPreviousJsonEncData != nil){
        NSData *data = [strPreviousJsonEncData dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"pushData %@", [json objectForKey:@"pushData"]);
        NSString *strPushData = [json objectForKey:@"pushData"];
        //NSString* status = [json objectForKey:@"status"];
        NSData *pushdata = [strPushData dataUsingEncoding:NSUTF8StringEncoding];
        id pushDatajson = [NSJSONSerialization JSONObjectWithData:pushdata options:0 error:nil];
        NSLog(@"pushDatajson %@", pushDatajson);
        
        txIP = [pushDatajson objectForKey:@"IP"];
        txAction = [pushDatajson objectForKey:@"Action"];
        txDate = [pushDatajson objectForKey:@"Date"];
        txMessage = [pushDatajson objectForKey:@"Message"];
        txExpiry = [pushDatajson objectForKey:@"Expiry Time"];
        txID = [pushDatajson objectForKey:@"pushTxId"];
        txLocation = [pushDatajson objectForKey:@"location"];
        
        // set transaction details
        [transactionDetails setTransactionIp:txIP];
        [transactionDetails setTransactionDate:txDate];
        [transactionDetails setTransactionMessage:txMessage];
        [transactionDetails setTransactionExpiryTime:txExpiry];
        [transactionDetails setTransactionID:txID];
        [transactionDetails setTransactionAction:txAction];
        //[transactionDetails setTransactionStatus:status];
        [transactionDetails setTransactionLocation:txLocation];
    }else{
        [transactionDetails setTransactionIp:txIP];
        [transactionDetails setTransactionDate:txDate];
        [transactionDetails setTransactionMessage:txMessage];
        [transactionDetails setTransactionExpiryTime:txExpiry];
        [transactionDetails setTransactionID:txID];
        [transactionDetails setTransactionAction:txAction];
        //[transactionDetails setTransactionStatus:txStatus];
        [transactionDetails setTransactionLocation:txLocation];
    }
    return transactionDetails;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)approveTx:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Confirm" message:@"Are You Sure" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction*  _Nonnull action){
                            NSLog(@"Ok clicked");
            NSString *response = self.approveTransaction;
            NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
            id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSString* strResult=[json objectForKey:@"result"];
            NSLog(@"strResult %@",strResult);
        if(strResult == nil){
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Server is not responding" preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
        }else if([strResult isEqualToString:@"success"]){
//            UIAlertController *failureAlertController =nil;
//            failureAlertController = [UIAlertController alertControllerWithTitle:@"Success" message:@"Your transaction is approved successfully" preferredStyle:UIAlertControllerStyleAlert];
//            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    SuccessViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"successTransaction"];
//                        vc.successMessage.text=@"Your transaction is approved successfully";
//                    [self presentViewController:vc animated:YES completion:nil];
//                });
//            }]];
            //[self presentViewController:failureAlertController animated:YES completion:nil];
            
            // history push
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *currentUserLogin = [prefs objectForKey:@"currentLogiedInUser"];
            NSString *stroedPreviousHistoryData = [self LoadFile:[@"historyPushDetails" stringByAppendingString:currentUserLogin]];
            NSString *strTxOneData = nil;
            NSString *strTxTwoData = nil;
            NSString *strTxThreeData = nil;
            NSString *strTxFourData = nil;
            NSString *strTxFiveData = nil;
            if(stroedPreviousHistoryData != nil){
                NSData *data = [stroedPreviousHistoryData dataUsingEncoding:NSUTF8StringEncoding];
                id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                strTxOneData = [json objectForKey:@"TxOne"];
                strTxTwoData = [json objectForKey:@"TxTwo"];
                strTxThreeData = [json objectForKey:@"TxThree"];
                strTxFourData = [json objectForKey:@"TxFour"];
                strTxFiveData = [json objectForKey:@"TxFive"];
            }
            //current Push
            NSString *stroedLivePushData = [self LoadFile:[@"livePushDetails" stringByAppendingString:currentUserLogin]];
            
            if(stroedLivePushData != nil){

                // Get Live Push Details
                NSData *livePushData = [stroedLivePushData dataUsingEncoding:NSUTF8StringEncoding];
                id livePushJson = [NSJSONSerialization JSONObjectWithData:livePushData options:0 error:nil];

                NSString *struserid = [livePushJson objectForKey:@"userid"];
                NSString *strPushData = [livePushJson objectForKey:@"pushData"];
                NSString *strapsData = [livePushJson objectForKey:@"apsData"];
                NSString *strcurrentDate = [livePushJson objectForKey:@"currentDate"];
                NSString* status=@"1";
                
                NSMutableDictionary *pStore = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                               struserid,@"userid",
                                               strPushData,@"pushData",
                                               strapsData ,@"apsData",
                                               strcurrentDate,@"currentDate",
                                               status,@"status",
                                               nil
                                               ];
                NSString *strJsonEncData = [self parseJSON:pStore];
                
                NSMutableDictionary *storePushHistory = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                               strJsonEncData,@"TxOne",
                                               strTxOneData,@"TxTwo",
                                               strTxTwoData,@"TxThree",
                                               strTxThreeData,@"TxFour",
                                               strTxFourData,@"TxFive",
                                               nil
                                               ];
                NSString *strJsonEncHistoryData = [self parseJSON:storePushHistory];
                int iResult = [self WriteFile:[@"historyPushDetails" stringByAppendingString:struserid] usingWith:strJsonEncHistoryData];
                 NSLog(@"File Write Result >> %d",iResult);
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                SuccessViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"successController"];
                vc.userId=[_userId stringByTrimmingCharactersInSet:
                           [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                [self presentViewController:vc animated:YES completion:nil];
                vc.successMessage.text=@"Your transaction is approved successfully";
            });
        } else if ([strResult isEqualToString:@"error"]){
                    UIAlertController *failureAlertController =nil;
                    failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:[json objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                    [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                    [self presentViewController:failureAlertController animated:YES completion:nil];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                ErrorViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"errorController"];
//
//                [self presentViewController:vc animated:YES completion:nil];
//                vc.errorLabel.text=[json objectForKey:@"message"];
//            });
           }
        }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

-(int) WriteFile:(NSString* )filename usingWith:(NSString* )filedata{
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/%@",
                          documentsDirectory,filename];
    //create content - four lines of text
    // NSString *content = @"One\nTwo\nThree\nFour\nFive";
    
    NSString *content = filedata;
    //save content to the documents directory
    bool result = [content writeToFile:fileName
                            atomically:NO
                              encoding:NSStringEncodingConversionAllowLossy
                                 error:nil];
    if(result == true){
        return 0;
    }else{
        return -1;
    }
    
}

-(NSString *)parseJSON:(NSMutableDictionary* )dictonary{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictonary options:NSJSONWritingPrettyPrinted error:&error];
    NSString *resultAsString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return resultAsString;
}

- (IBAction)denyTx:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Confirm" message:@"Are You Sure" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction*  _Nonnull action){        
        NSString *response = self.denyTransaction;
        NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSString* strResult=[json objectForKey:@"result"];
        NSLog(@"strResult %@",strResult);
        if(strResult == nil){
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Server is not responding" preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
        }else if([strResult isEqualToString:@"success"]){
//            UIAlertController *failureAlertController =nil;
//            failureAlertController = [UIAlertController alertControllerWithTitle:@"Success" message:@"Your transaction is denied successfully" preferredStyle:UIAlertControllerStyleAlert];
//            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
//            [self presentViewController:failureAlertController animated:YES completion:nil];
            dispatch_async(dispatch_get_main_queue(), ^{
                SuccessViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"successTransaction"];
                
                [self presentViewController:vc animated:YES completion:nil];
                vc.successMessage.text=@"Your transaction is approved successfully";
            });
        } else if ([strResult isEqualToString:@"error"]){
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:[json objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                ErrorViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"errorController"];
//
//                [self presentViewController:vc animated:YES completion:nil];
//                vc.errorLabel.text=[json objectForKey:@"message"];
//            });
        }
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];


}

-(NSString*) approveTransaction{
    NSLog(@"%@",txId);
    NSString *uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *strdata =[NSString stringWithFormat:@"{\"transactionResponse\":\"%d\",\"_deviceid\":\"%@\"}",3,uniqueIdentifier];
    NSCharacterSet * queryKVSet = [NSCharacterSet
                                   characterSetWithCharactersInString:@"+"
                                   ].invertedSet;
    
    NSString * parameterString = [NSString                                  stringWithFormat:@"transactionId=%@&devicepayload=%@",
                                  txId,strdata
                                  ];
    NSLog(@"%@", parameterString);
    NSString* encodedUrl = [parameterString stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet];
    
    NSData *postData = [encodedUrl dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
    
    NSString *postLength=[NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    Details* details = [[Details alloc]init];
    NSString* urlDetails = [details getURL];
    NSString* post_url= [urlDetails stringByAppendingString:@"/UpdateTransactionStatus"];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@",post_url]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString* aStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] ;
    NSLog(@"responseData %@",aStr);
    return aStr;
}

-(NSString*) denyTransaction{
    NSLog(@"%@",txId);
    NSString *uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *strdata =[NSString stringWithFormat:@"{\"transactionResponse\":\"%d\",\"_deviceid\":\"%@\"}",4,uniqueIdentifier];
    NSCharacterSet * queryKVSet = [NSCharacterSet
                                   characterSetWithCharactersInString:@"+"
                                   ].invertedSet;
    
    NSString * parameterString = [NSString                                  stringWithFormat:@"transactionId=%@&devicepayload=%@",
                                  txId,strdata
                                  ];
    NSLog(@"%@", parameterString);
    NSString* encodedUrl = [parameterString stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet];
    
    NSData *postData = [encodedUrl dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
    
    NSString *postLength=[NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    Details* details = [[Details alloc]init];
    NSString* urlDetails = [details getURL];
    NSString* post_url= [urlDetails stringByAppendingString:@"/UpdateTransactionStatus"];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@",post_url]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString* aStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] ;
    NSLog(@"responseData %@",aStr);
    return aStr;
}

@end
