//
//  TransactionDetails.m
//  AcledaPush
//
//  Created by Abhishek Ingle on 22/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import "TransactionDetails.h"

@implementation TransactionDetails

@synthesize transactionIp = _transactionIp;
@synthesize transactionDate = _transactionDate;
@synthesize transactionTime = _transactionTime;
@synthesize transactionExpiryTime = _transactionExpiryTime;
@synthesize transactionMessage = _transactionMessage;
@synthesize transactionBrowser = _transactionBrowser;
@synthesize transactionOS = _transactionOS;
@synthesize transactionID = _transactionID;
@synthesize transactionAction = _transactionAction;
@synthesize transactionStatus = _transactionStatus;
@synthesize transactionLocation = _transactionLocation;

@synthesize transactionIpTwo = _transactionIpTwo;
@synthesize transactionDateTwo = _transactionDateTwo;
@synthesize transactionTimeTwo = _transactionTimeTwo;
@synthesize transactionExpiryTimeTwo = _transactionExpiryTimeTwo;
@synthesize transactionMessageTwo = _transactionMessageTwo;
@synthesize transactionBrowserTwo = _transactionBrowserTwo;
@synthesize transactionOSTwo = _transactionOSTwo;
@synthesize transactionIDTwo = _transactionIDTwo;
@synthesize transactionActionTwo = _transactionActionTwo;
@synthesize transactionStatusTwo = _transactionStatusTwo;
@synthesize transactionLocationTwo = _transactionLocationTwo;

@synthesize transactionIpThree = _transactionIpThree;
@synthesize transactionDateThree = _transactionDateThree;
@synthesize transactionTimeThree = _transactionTimeThree;
@synthesize transactionExpiryTimeThree = _transactionExpiryTimeThree;
@synthesize transactionMessageThree = _transactionMessageThree;
@synthesize transactionBrowserThree = _transactionBrowserThree;
@synthesize transactionOSThree = _transactionOSThree;
@synthesize transactionIDThree = _transactionIDThree;
@synthesize transactionActionThree = _transactionActionThree;
@synthesize transactionStatusThree = _transactionStatusThree;
@synthesize transactionLocationThree = _transactionLocationThree;

@synthesize transactionIpFour = _transactionIpFour;
@synthesize transactionDateFour = _transactionDateFour;
@synthesize transactionTimeFour = _transactionTimeFour;
@synthesize transactionExpiryTimeFour = _transactionExpiryTimeFour;
@synthesize transactionMessageFour = _transactionMessageFour;
@synthesize transactionBrowserFour = _transactionBrowserFour;
@synthesize transactionOSFour = _transactionOSFour;
@synthesize transactionIDFour = _transactionIDFour;
@synthesize transactionActionFour = _transactionActionFour;
@synthesize transactionStatusFour = _transactionStatusFour;
@synthesize transactionLocationFour = _transactionLocationFour;

@synthesize transactionIpFive = _transactionIpFive;
@synthesize transactionDateFive = _transactionDateFive;
@synthesize transactionTimeFive = _transactionTimeFive;
@synthesize transactionExpiryTimeFive = _transactionExpiryTimeFive;
@synthesize transactionMessageFive = _transactionMessageFive;
@synthesize transactionBrowserFive = _transactionBrowserFive;
@synthesize transactionOSFive = _transactionOSFive;
@synthesize transactionIDFive = _transactionIDFive;
@synthesize transactionActionFive = _transactionActionFive;
@synthesize transactionStatusFive = _transactionStatusFive;
@synthesize transactionLocationFive = _transactionLocationFive;

- (void) setTransactionLocationFive:(NSString* )transactionLocationFive {
    _transactionLocationFive = transactionLocationFive;
}
//Getter method
- (NSString *) transactionLocationFive {
    return _transactionLocationFive;
}
- (void) setTransactionStatusFive:(NSString* )transactionStatusFive {
    _transactionStatusFive = transactionStatusFive;
}
//Getter method
- (NSString *) transactionStatusFive {
    return _transactionStatusFive;
}
- (void) setTransactionIDFive:(NSString* )transactionIDFive {
    _transactionIDFive = transactionIDFive;
}
//Getter method
- (NSString *) transactionIDFive {
    return _transactionIDFive;
}
- (void) setTransactionOSFive:(NSString* )transactionOSFive {
    _transactionOSFive = transactionOSFive;
}
//Getter method
- (NSString *) transactionOSFive {
    return _transactionOSFive;
}
- (void) setTransactionBrowserFive:(NSString* )transactionBrowserFive {
    _transactionBrowserFive = transactionBrowserFive;
}
//Getter method
- (NSString *) transactionBrowserFive {
    return _transactionBrowserFive;
}
- (void) setTransactionMessageFive:(NSString* )transactionMessageFive{
    _transactionMessageFive = transactionMessageFive;
}
//Getter method
- (NSString *) transactionMessageFive {
    return _transactionMessageFive;
}
- (void) setTransactionExpiryTimeFive:(NSString* )transactionExpiryTimeFive {
    _transactionExpiryTimeFive = transactionExpiryTimeFive;
}
//Getter method
- (NSString *) transactionExpiryTimeFive {
    return _transactionExpiryTimeFive;
}
- (void) setTransactionTimeFive:(NSString* )transactionTimeFive {
    _transactionTimeFive = transactionTimeFive;
}
//Getter method
- (NSString *) transactionTimeFive {
    return _transactionTimeFive;
}
- (void) setTransactionDateFive:(NSString* )transactionDateFive {
    _transactionDateFive = transactionDateFive;
}
//Getter method
- (NSString *) transactionDateFive{
    return _transactionDateFive;
}
- (void) setTransactionIpFive:(NSString* )transactionIpFive {
    _transactionIpFive = transactionIpFive;
}
//Getter method
- (NSString *) transactionIpFive {
    return _transactionIpFive;
}
- (void) setTransactionActionFive:(NSString* )transactionActionFive {
    _transactionActionFive = transactionActionFive;
}
//Getter method
- (NSString *) transactionActionFive {
    return _transactionActionFive;
}



- (void) setTransactionLocationFour:(NSString* )transactionLocationFour {
    _transactionLocationFour = transactionLocationFour;
}
//Getter method
- (NSString *) transactionLocationFour {
    return _transactionLocationFour;
}
- (void) setTransactionStatusFour:(NSString* )transactionStatusFour {
    _transactionStatusFour = transactionStatusFour;
}
//Getter method
- (NSString *) transactionStatusFour {
    return _transactionStatusFour;
}
- (void) setTransactionIDFour:(NSString* )transactionIDFour {
    _transactionIDFour = transactionIDFour;
}
//Getter method
- (NSString *) transactionIDFour {
    return _transactionIDFour;
}
- (void) setTransactionOSFour:(NSString* )transactionOSFour {
    _transactionOSThree = transactionOSFour;
}
//Getter method
- (NSString *) transactionOSFour {
    return _transactionOSFour;
}
- (void) setTransactionBrowserFour:(NSString* )transactionBrowserFour {
    _transactionBrowserFour = transactionBrowserFour;
}
//Getter method
- (NSString *) transactionBrowserFour {
    return _transactionBrowserFour;
}
- (void) setTransactionMessageFour:(NSString* )transactionMessageFour{
    _transactionMessageFour = transactionMessageFour;
}
//Getter method
- (NSString *) transactionMessageFour {
    return _transactionMessageFour;
}
- (void) setTransactionExpiryTimeFour:(NSString* )transactionExpiryTimeFour {
    _transactionExpiryTimeFour = transactionExpiryTimeFour;
}
//Getter method
- (NSString *) transactionExpiryTimeFour {
    return _transactionExpiryTimeFour;
}
- (void) setTransactionTimeFour:(NSString* )transactionTimeFour {
    _transactionTimeFour = transactionTimeFour;
}
//Getter method
- (NSString *) transactionTimeFour {
    return _transactionTimeFour;
}
- (void) setTransactionDateFour:(NSString* )transactionDateFour {
    _transactionDateFour = transactionDateFour;
}
//Getter method
- (NSString *) transactionDateFour{
    return _transactionDateFour;
}
- (void) setTransactionIpFour:(NSString* )transactionIpFour {
    _transactionIpFour = transactionIpFour;
}
//Getter method
- (NSString *) transactionIpFour {
    return _transactionIpFour;
}
- (void) setTransactionActionFour:(NSString* )transactionActionFour {
    _transactionActionFour = transactionActionFour;
}
//Getter method
- (NSString *) transactionActionFour {
    return _transactionActionFour;
}

- (void) setTransactionLocationThree:(NSString* )transactionLocationThree {
    _transactionLocationThree = transactionLocationThree;
}
//Getter method
- (NSString *) transactionLocationThree {
    return _transactionLocationThree;
}
- (void) setTransactionStatusThree:(NSString* )transactionStatusThree {
    _transactionStatusThree = transactionStatusThree;
}
//Getter method
- (NSString *) transactionStatusThree {
    return _transactionStatusThree;
}
- (void) setTransactionIDThree:(NSString* )transactionIDThree {
    _transactionIDThree = transactionIDThree;
}
//Getter method
- (NSString *) transactionIDThree {
    return _transactionIDThree;
}
- (void) setTransactionOSThree:(NSString* )transactionOSThree {
    _transactionOSThree = transactionOSThree;
}
//Getter method
- (NSString *) transactionOSThree {
    return _transactionOSThree;
}
- (void) setTransactionBrowserThree:(NSString* )transactionBrowserThree {
    _transactionBrowserThree = transactionBrowserThree;
}
//Getter method
- (NSString *) transactionBrowserThree {
    return _transactionBrowserThree;
}
- (void) setTransactionMessageThree:(NSString* )transactionMessageThree{
    _transactionMessageThree = transactionMessageThree;
}
//Getter method
- (NSString *) transactionMessageThree {
    return _transactionMessageThree;
}
- (void) setTransactionExpiryTimeThree:(NSString* )transactionExpiryTimeThree {
    _transactionExpiryTimeThree = transactionExpiryTimeThree;
}
//Getter method
- (NSString *) transactionExpiryTimeThree {
    return _transactionExpiryTimeThree;
}
- (void) setTransactionTimeThree:(NSString* )transactionTimeThree {
    _transactionTimeThree = transactionTimeThree;
}
//Getter method
- (NSString *) transactionTimeThree {
    return _transactionTimeThree;
}
- (void) setTransactionDateThree:(NSString* )transactionDateThree {
    _transactionDateThree = transactionDateThree;
}
//Getter method
- (NSString *) transactionDateThree{
    return _transactionDateThree;
}
- (void) setTransactionIpThree:(NSString* )transactionIpThree {
    _transactionIpThree = transactionIpThree;
}
//Getter method
- (NSString *) transactionIpThree {
    return _transactionIpThree;
}
- (void) setTransactionActionThree:(NSString* )transactionActionThree {
    _transactionActionThree = transactionActionThree;
}
//Getter method
- (NSString *) transactionActionThree {
    return _transactionActionThree;
}

- (void) setTransactionLocationTwo:(NSString* )transactionLocationTwo {
    _transactionLocationTwo = transactionLocationTwo;
}
//Getter method
- (NSString *) transactionLocationTwo {
    return _transactionLocationTwo;
}
- (void) setTransactionStatusTwo:(NSString* )transactionStatusTwo {
    _transactionStatusTwo = transactionStatusTwo;
}
//Getter method
- (NSString *) transactionStatusTwo {
    return _transactionStatusTwo;
}
- (void) setTransactionIDTwo:(NSString* )transactionIDTwo {
    _transactionIDTwo = transactionIDTwo;
}
//Getter method
- (NSString *) transactionIDTwo {
    return _transactionIDTwo;
}
- (void) setTransactionOSTwo:(NSString* )transactionOSTwo {
    _transactionOSTwo = transactionOSTwo;
}
//Getter method
- (NSString *) transactionOSTwo {
    return _transactionOSTwo;
}
- (void) setTransactionBrowserTwo:(NSString* )transactionBrowserTwo {
    _transactionBrowserTwo = transactionBrowserTwo;
}
//Getter method
- (NSString *) transactionBrowserTwo {
    return _transactionBrowserTwo;
}
- (void) setTransactionMessageTwo:(NSString* )transactionMessageTwo {
    _transactionMessageTwo = transactionMessageTwo;
}
//Getter method
- (NSString *) transactionMessageTwo {
    return _transactionMessageTwo;
}
- (void) setTransactionExpiryTimeTwo:(NSString* )transactionExpiryTimeTwo {
    _transactionExpiryTimeTwo = transactionExpiryTimeTwo;
}
//Getter method
- (NSString *) transactionExpiryTimeTwo {
    return _transactionExpiryTimeTwo;
}
- (void) setTransactionTimeTwo:(NSString* )transactionTimeTwo {
    _transactionTimeTwo = transactionTimeTwo;
}
//Getter method
- (NSString *) transactionTimeTwo {
    return _transactionTimeTwo;
}
- (void) setTransactionDateTwo:(NSString* )transactionDateTwo {
    _transactionDateTwo = transactionDateTwo;
}
//Getter method
- (NSString *) transactionDateTwo {
    return _transactionDateTwo;
}
- (void) setTransactionIpTwo:(NSString* )transactionIpTwo {
    _transactionIpTwo = transactionIpTwo;
}
//Getter method
- (NSString *) transactionIpTwo {
    return _transactionIpTwo;
}
- (void) setTransactionActionTwo:(NSString* )transactionActionTwo {
    _transactionActionTwo = transactionActionTwo;
}
//Getter method
- (NSString *) transactionActionTwo {
    return _transactionActionTwo;
}


- (void) setTransactionAction:(NSString* )transactionAction {
    _transactionAction = transactionAction;
}
//Getter method
- (NSString *) transactionAction {
    return _transactionAction;
}

- (void) setTransactionLocation:(NSString* )transactionLocation {
    _transactionLocation = transactionLocation;
}
//Getter method
- (NSString *) transactionLocation {
    return _transactionLocation;
}

- (void) setTransactionStatus:(NSString* )transactionStatus {
    _transactionStatus = transactionStatus;
}
//Getter method
- (NSString *) transactionStatus {
    return _transactionStatus;
}

- (void) setTransactionIp:(NSString* )transactionIp {
    _transactionIp = transactionIp;
}
//Getter method
- (NSString *) transactionIp {
    return _transactionIp;
}

- (void) setTransactionID:(NSString* )transactionID {
    _transactionID = transactionID;
}
//Getter method
- (NSString *) transactionID {
    return _transactionID;
}

- (void) setTransactionDate:(NSString *)transactionDate {
    _transactionDate = transactionDate;
}
//Getter method
- (NSString *) transactionDate {
    return _transactionDate;
}

- (void) setTransactionTime:(NSString *)transactionTime {
    _transactionTime = transactionTime;
}
//Getter method
- (NSString *) transactionTime {
    return _transactionTime;
}

- (void) setTransactionExpiryTime:(NSString *)transactionExpiryTime {
    _transactionExpiryTime = transactionExpiryTime;
}
//Getter method
- (NSString *) transactionExpiryTime {
    return _transactionExpiryTime;
}

- (void) setTransactionMessage:(NSString *)transactionMessage {
    _transactionMessage = transactionMessage;
}
//Getter method
- (NSString *) transactionMessage {
    return _transactionMessage;
}

- (void) setTransactionBrowser:(NSString *)transactionBrowser {
    _transactionBrowser = transactionBrowser;
}
//Getter method
- (NSString *) transactionBrowser {
    return _transactionBrowser;
}

- (void) setTransactionOS:(NSString *)transactionOS {
    _transactionOS = transactionOS;
}
//Getter method
- (NSString *) transactionOS {
    return _transactionOS;
}
@end
