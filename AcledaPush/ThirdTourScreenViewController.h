//
//  ThirdTourScreenViewController.h
//  AcledaPush
//
//  Created by Abhishek Ingle on 02/04/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThirdTourScreenViewController : UIViewController
- (IBAction)thirdTourFinish:(id)sender;

@end
