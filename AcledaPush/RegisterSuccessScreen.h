//
//  RegisterSuccessScreen.h
//  AcledaPush
//
//  Created by Abhishek Ingle on 28/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterSuccessScreen : UIViewController
- (IBAction)goTOLoginScreen:(id)sender;
- (IBAction)registerAnotherUser:(id)sender;

@end
