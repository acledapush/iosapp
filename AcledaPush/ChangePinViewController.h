//
//  ChangePinViewController.h
//  AcledaPush
//
//  Created by Abhishek Ingle on 25/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePinViewController : UIViewController

@property (weak,nonatomic) IBOutlet UIBarButtonItem *barButton;
@property (weak, nonatomic) IBOutlet UITextField *currentPIN;

@property (weak, nonatomic) IBOutlet UITextField *confirmAuthMPIN;

@property (weak,nonatomic) IBOutlet UITextField *confirmAuthMPIN1;

@property (weak, nonatomic) IBOutlet UITextField *currentPinOne;
@property (weak, nonatomic) IBOutlet UITextField *currentPinTwo;
@property (weak, nonatomic) IBOutlet UITextField *currentPinThree;
@property (weak, nonatomic) IBOutlet UITextField *currentPinFour;

@property (weak, nonatomic) IBOutlet UITextField *nPINOne;
@property (weak, nonatomic) IBOutlet UITextField *nPINTwo;

@property (weak, nonatomic) IBOutlet UITextField *nPINThree;
@property (weak, nonatomic) IBOutlet UITextField *nPINFour;

@property (weak, nonatomic) IBOutlet UITextField *confirmPINOne;
@property (weak, nonatomic) IBOutlet UITextField *confirmPINTwo;
@property (weak, nonatomic) IBOutlet UITextField *confirmPINThree;
@property (weak, nonatomic) IBOutlet UITextField *confirmPINFour;


- (IBAction)cancelChnageMPIN:(id)sender;

- (IBAction)saveMPIN:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *userIdForChangePin;


@end
