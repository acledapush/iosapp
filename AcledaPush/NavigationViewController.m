//
//  NavigationViewController.m
//  AcledaPush
//
//  Created by Abhishek Ingle on 25/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import "NavigationViewController.h"
#import "SWRevealViewController.h"
#import "LoginController.h"
#import "AppDelegate.h"
#import "HeadingTableTableViewCell.h"

@interface NavigationViewController ()

@end

@implementation NavigationViewController{
    NSArray *menu;
}
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    menu = @[@"heading",@"first",@"second",@"third"];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [menu count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        return 200;
    }
    return 40;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [menu objectAtIndex:indexPath.row];
    UITableViewCell *cell;
    int menuRow = (int)indexPath.row + 1;
    if(menuRow == 1){
    HeadingTableTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        cell.userIdCellDetails.text = [prefs objectForKey:@"currentLogiedInUser"];
        return cell;
    }
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if(menuRow == 4){
        UIButton *addFriendButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        addFriendButton.frame = CGRectMake(60.0f, 5.0f, 75.0f, 30.0f);
        [addFriendButton setTitle:@" Log out " forState:UIControlStateNormal];
        [addFriendButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        addFriendButton.titleLabel.font = [UIFont systemFontOfSize:18.0];
        [cell addSubview:addFriendButton];
        [addFriendButton addTarget:self
                            action:@selector(logout:)
                  forControlEvents:UIControlEventTouchUpInside];
    }

    
    // Configure the cell...
    
    return cell;
}

- (IBAction)logout:(id)sender
{
    //NSLog(@"Add friend.");
    //if (buttonIndex == 0) {
//        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
//        [defaults setObject:nil forKey:@"UserId"];
//        [defaults synchronize];
        //redirect to login view
        
    dispatch_async(dispatch_get_main_queue(), ^{
        LoginController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginToPush"];        
        [self presentViewController:vc animated:YES completion:nil];
    });
        
    //}
}
- (IBAction)changePIN:(id)sender
{
    [self showLockViewForChangingPasscode];

}
- (void)showLockViewForChangingPasscode {
//    [[LTHPasscodeViewController sharedUser] showForChangingPasscodeInViewController:self asModal:NO];
}
#pragma mark - Navigation

 //In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] ) {
        SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
        
        swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
            
            UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
            [navController setViewControllers: @[dvc] animated: NO ];
            
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        };
        
    }
}


@end
