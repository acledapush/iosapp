//
//  SetMPINController.h
//  AcledaPush
//
//  Created by Abhishek Ingle on 16/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetMPINController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *mpin;
- (IBAction)setMPIN:(id)sender;

@property ( nonatomic, strong ) NSString *userId;

@property (weak, nonatomic) IBOutlet UITextField *setMPINOne;
@property (weak, nonatomic) IBOutlet UITextField *setMPINTwo;
@property (weak, nonatomic) IBOutlet UITextField *setMPINThree;
@property (weak, nonatomic) IBOutlet UITextField *setMPINFour;
@property (weak, nonatomic) IBOutlet UITextField *confirmSMPINOne;
@property (weak, nonatomic) IBOutlet UITextField *confirmSMPINTwo;
@property (weak, nonatomic) IBOutlet UITextField *confirmSMPINThree;
@property (weak, nonatomic) IBOutlet UITextField *confirmSMPINFour;


@end
