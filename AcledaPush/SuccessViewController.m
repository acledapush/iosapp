//
//  SuccessViewController.m
//  AcledaPush
//
//  Created by Abhishek Ingle on 24/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import "SuccessViewController.h"
#import "SWRevealViewController.h"
@interface SuccessViewController ()

@end

@implementation SuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    _barButton.target = self.revealViewController;
//    _barButton.action = @selector(revealToggle:);
//    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)homeRedirect:(id)sender {
    SWRevealViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"homeScreen"];
    vc.userId=[_userId stringByTrimmingCharactersInSet:
               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    [self presentViewController:vc animated:YES completion:nil];
}
@end
