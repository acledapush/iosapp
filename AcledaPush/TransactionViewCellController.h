//
//  TransactionViewCellController.h
//  AcledaPush
//
//  Created by Abhishek Ingle on 23/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionViewCellController : UIViewController

- (IBAction)approveTx:(id)sender;
- (IBAction)denyTx:(id)sender;

@property ( nonatomic, strong ) NSString *userId;


@end
