//
//  LoginController.m
//  AcledaPush
//
//  Created by Abhishek Ingle on 16/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import "LoginController.h"
#import "Details.h"
#import "TransactionViewCellController.h"
#import "LoginPushViewController.h"
#import "SWRevealViewController.h"

@interface LoginController ()

@end

@implementation LoginController

static NSString *keyboardOpened=nil;
+(NSString*)keyboardOpened{return  keyboardOpened;}
+(void)setKeyboardOpened:(NSString*)value{
    keyboardOpened=value;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [_firstDigitOfMPIN addTarget:self action:@selector(firstDigitOfPinChanged) forControlEvents:UIControlEventEditingChanged];
    [_secondDigitOfMPIN addTarget:self action:@selector(secondDigitOfPinChanged) forControlEvents:UIControlEventEditingChanged];
    [_thirdDigitOfMPIN addTarget:self action:@selector(thirdDigitOfPinChanged) forControlEvents:UIControlEventEditingChanged];
    [_fourthDigitOfMPIN addTarget:self action:@selector(fourthDigitOfPinChanged) forControlEvents:UIControlEventEditingChanged];
    //_loginUserId.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fourthDigitOfPinChanged{
    NSString* pinFour = _fourthDigitOfMPIN.text;
    if(![pinFour isEqualToString:@""]){
        _fourthDigitOfMPIN.secureTextEntry = YES;
        [self nextFourthDigitOfPinFocus];
    }
}
- (void)thirdDigitOfPinChanged{
    NSString* pinThree = _thirdDigitOfMPIN.text;
    if(![pinThree isEqualToString:@""]){
        _thirdDigitOfMPIN.secureTextEntry = YES;
        [self nextThirdDigitOfPinFocus];
    }
}
- (void)secondDigitOfPinChanged{
    NSString* pinTwo = _secondDigitOfMPIN.text;
    if(![pinTwo isEqualToString:@""]){
        _secondDigitOfMPIN.secureTextEntry = YES;
        [self nextSecondDigitOfPinFocus];
    }
}
- (void)firstDigitOfPinChanged{
    NSString* pinOne = _firstDigitOfMPIN.text;
    if(![pinOne isEqualToString:@""]){
        _firstDigitOfMPIN.secureTextEntry = YES;
        [self nextFirstDigitOfPinFocus];
    }
}

-(void) nextFirstDigitOfPinFocus {
    [_secondDigitOfMPIN becomeFirstResponder];
}

-(void) nextSecondDigitOfPinFocus {
    [_thirdDigitOfMPIN becomeFirstResponder];
}

-(void) nextThirdDigitOfPinFocus {
    [_fourthDigitOfMPIN becomeFirstResponder];
}
-(void) nextFourthDigitOfPinFocus{
    [_fourthDigitOfMPIN resignFirstResponder];
}

-(BOOL) textFieldShouldReturn: (UITextField *) textField
{
    [textField resignFirstResponder];
    if(keyboardOpened!=nil && [keyboardOpened isEqualToString:@"YES"]){
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
        keyboardOpened=nil;
    }
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField== _firstDigitOfMPIN || textField== _secondDigitOfMPIN || textField== _thirdDigitOfMPIN || textField== _fourthDigitOfMPIN){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        keyboardOpened=@"YES";
    }
    
    return YES;
}
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -keyboardSize.height;
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
}

// This allows numeric text only, but also backspace for deletes
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _loginUserId){
        NSRange lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
        
        if (lowercaseCharRange.location != NSNotFound) {
            
            if (textField.text.length == 0) {
                
                // Add first character -- and update Return button
                //
                // But this has the effect of losing the editing point
                // (only when trying to edit with lowercase characters),
                // because the text of the UITextField is modified,
                // which does not matter since the caret is at the end
                // of the (empty) text fiueld anyways.
                //
                textField.text = [textField.text stringByReplacingCharactersInRange:range withString:[string uppercaseString]];
                
            } else {
                
                // Replace range instead of whole string in order
                // not to lose input caret position -- but this will
                // not update the Return button, which is not necessary
                // here anyways
                //
                UITextPosition *beginning = textField.beginningOfDocument;
                UITextPosition *start = [textField positionFromPosition:beginning offset:range.location];
                UITextPosition *end = [textField positionFromPosition:start offset:range.length];
                UITextRange *textRange = [textField textRangeFromPosition:start toPosition:end];
                
                [textField replaceRange:textRange withText:[string uppercaseString]];
            }
            
            return NO;
        }
        return YES;
    }
NSUInteger oldLength = [textField.text length];
NSUInteger replacementLength = [string length];
NSUInteger rangeLength = range.length;

NSUInteger newLength = oldLength - rangeLength + replacementLength;

// This 'tabs' to next field when entering digits
if (newLength == 1) {
    if (textField == _firstDigitOfMPIN)
    {
        [self performSelector:@selector(setNextResponder:) withObject:_secondDigitOfMPIN afterDelay:0.2];
    }
    else if (textField == _secondDigitOfMPIN)
    {
        [self performSelector:@selector(setNextResponder:) withObject:_thirdDigitOfMPIN afterDelay:0.2];
    }
    else if (textField == _thirdDigitOfMPIN)
    {
        [self performSelector:@selector(setNextResponder:) withObject:_fourthDigitOfMPIN afterDelay:0.2];
    }
}
//this goes to previous field as you backspace through them, so you don't have to tap into them individually
else if (oldLength > 0 && newLength == 0) {
    if (textField == _fourthDigitOfMPIN)
    {
        [self performSelector:@selector(setNextResponder:) withObject:_thirdDigitOfMPIN afterDelay:0.1];
    }
    else if (textField == _thirdDigitOfMPIN)
    {
        [self performSelector:@selector(setNextResponder:) withObject:_secondDigitOfMPIN afterDelay:0.1];
    }
    else if (textField == _secondDigitOfMPIN)
    {
        [self performSelector:@selector(setNextResponder:) withObject:_firstDigitOfMPIN afterDelay:0.1];
    }
}
    return newLength <= 1;
}

- (void)setNextResponder:(UITextField *)nextResponder
{
    [nextResponder becomeFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
UIAlertController *progressController;
- (IBAction)loginUser:(id)sender {
     progressController = [UIAlertController alertControllerWithTitle:@"Please wait" message:@"Login is in process ..." preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:progressController animated:YES completion:nil];
    dispatch_async(dispatch_get_main_queue(), ^{[self checkLogin];});
}

-(void) checkLogin{
    NSString *stloginUserId = _loginUserId.text;
    //NSString *stloginMPIN = _loginMPIN.text;
    NSString *stloginMPIN = [NSString stringWithFormat:@"%@%@%@%@",_firstDigitOfMPIN.text,_secondDigitOfMPIN.text,_thirdDigitOfMPIN.text,_fourthDigitOfMPIN.text];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *savedValue = [prefs objectForKey:stloginUserId];
    NSString *pushForUser = [prefs objectForKey:@"pushForTheUser"];
    if([savedValue isEqualToString:stloginMPIN]){
        //[failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        
        NSUserDefaults *currentUL = [NSUserDefaults standardUserDefaults];
        [currentUL setObject:stloginUserId forKey:@"currentLogiedInUser"];
        // NSLog(@"userMPIN %@",[prefs stringForKey:_userId]);
        [currentUL synchronize];
        NSMutableDictionary *pAction = Details.pushAction;
        // Incase user login when push not come
        if(pAction == nil){
            [self dismissViewControllerAnimated:progressController completion:^{
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:[stloginUserId stringByAppendingString:@"failedLoginAttempt"]];
//                UIAlertController *failureAlertController = [UIAlertController alertControllerWithTitle:@"Success" message:@"Login Successful" preferredStyle:UIAlertControllerStyleAlert];
                
                //[NSThread sleepForTimeInterval:1.0f];
//                [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        SWRevealViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"homeScreen"];
                        vc.userId=[stloginUserId stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        [self presentViewController:vc animated:YES completion:nil];
                    });
//                }]];
//                [self presentViewController:failureAlertController animated:YES completion:nil];
            }];
        }
        //user login when push come
        if(pAction != nil){
            NSLog(@"%@",[pAction objectForKey:pushForUser]);
            NSString *userActionstr = [pAction objectForKey:pushForUser];
            NSError * err;
            NSData *data =[userActionstr dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary * userAction;
            if(data!=nil){
                userAction = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
            }
            if(userAction != nil){
                NSString *pActionUser = [userAction objectForKey:@"userid"];
                NSString *pActionDetails = [userAction objectForKey:@"resultsAction"];
                // check push come for that user that user only login
                if([pActionUser isEqualToString:stloginUserId]){
                    
                    NSUserDefaults *currentUL = [NSUserDefaults standardUserDefaults];
                    [currentUL setObject:stloginUserId forKey:@"currentLogiedInUser"];
                   // NSLog(@"userMPIN %@",[prefs stringForKey:_userId]);
                    [currentUL synchronize];
                        [self dismissViewControllerAnimated:progressController completion:^{
                            Details.pushAction = nil;
//                            UIAlertController *failureAlertController =nil;
//                            failureAlertController = [UIAlertController alertControllerWithTitle:@"Success" message:@"Login Successful" preferredStyle:UIAlertControllerStyleAlert];
//                            [self presentViewController:failureAlertController animated:YES completion:nil];
//                            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    LoginPushViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"loginPush"];
                                    vc.userId=[stloginUserId stringByTrimmingCharactersInSet:
                                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                    [self presentViewController:vc animated:YES completion:nil];
                                    if([pActionDetails isEqualToString:@"L"]){
                                        vc.transactionHeading.text=@"Login Details";
                                    }else{
                                        vc.transactionHeading.text=@"Transaction Details";
                                    }
                                });
//                            }]];
                        }];
                }else{
                    [self dismissViewControllerAnimated:progressController completion:^{
                        UIAlertController *failureAlertController =nil;
                        failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"You are not valid user to view this push" preferredStyle:UIAlertControllerStyleAlert];
                        [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                        [self presentViewController:failureAlertController animated:YES completion:nil];
                    }];
                }
            }
        }
    }else{
        //reset user if he register for mpin
        if(savedValue != nil){
           
            NSUserDefaults *userWroungAttempt = [NSUserDefaults standardUserDefaults];
           // NSInteger savedLoginFailedAttemptValue = [userWroungAttempt integerForKey:[stloginUserId stringByAppendingString:@"failedLoginAttempt"]];
            NSInteger savedLoginFailedAttemptValue = 0;
            
            if([[[userWroungAttempt dictionaryRepresentation] allKeys] containsObject:[stloginUserId stringByAppendingString:@"failedLoginAttempt"]]){
                
                //NSLog(@"mykey found");
                savedLoginFailedAttemptValue = [userWroungAttempt integerForKey:[stloginUserId stringByAppendingString:@"failedLoginAttempt"]];
                if(savedLoginFailedAttemptValue >= 5){
                    [userWroungAttempt removeObjectForKey:[stloginUserId stringByAppendingString:@"failedLoginAttempt"]];
                    [userWroungAttempt setInteger:1 forKey:[stloginUserId stringByAppendingString:@"failedLoginAttempt"]];
                    [userWroungAttempt synchronize];
                    savedLoginFailedAttemptValue = 0;
                }
            }
            
            NSInteger loginFailedCount = 0;
            if(savedLoginFailedAttemptValue != nil){
                if(savedLoginFailedAttemptValue == 3){
                    savedLoginFailedAttemptValue = savedLoginFailedAttemptValue+1;
                    [userWroungAttempt setInteger:savedLoginFailedAttemptValue forKey:[stloginUserId stringByAppendingString:@"failedLoginAttempt"]];
                    [userWroungAttempt synchronize];
                     [self dismissViewControllerAnimated:progressController completion:^{
                        UIAlertController *failureAlertController =nil;
                        failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"This is your last attempt, If failed your assign token will be reset." preferredStyle:UIAlertControllerStyleAlert];
                        [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                        [self presentViewController:failureAlertController animated:YES completion:nil];
                     }];
                }else{
                    savedLoginFailedAttemptValue = savedLoginFailedAttemptValue+1;
                    if(savedLoginFailedAttemptValue == 5){
                        NSString *response = self.resetUser;
                        NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                        NSString* strResult=[json objectForKey:@"result"];
                        NSLog(@"strResult %@",strResult);
                        if([strResult isEqualToString:@"success"]){
                            [userWroungAttempt setInteger:savedLoginFailedAttemptValue forKey:[stloginUserId stringByAppendingString:@"failedLoginAttempt"]];
                            [userWroungAttempt synchronize];
                            
                            [prefs removeObjectForKey:stloginUserId];
                            [self dismissViewControllerAnimated:progressController completion:^{
                                UIAlertController *failureAlertController =nil;
                                failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Your Push token reset" preferredStyle:UIAlertControllerStyleAlert];
                                [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                                [self presentViewController:failureAlertController animated:YES completion:nil];
                            }];
                        }else{
                            [self dismissViewControllerAnimated:progressController completion:^{
                                UIAlertController *failureAlertController =nil;
                                failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Wrong User ID or PIN" preferredStyle:UIAlertControllerStyleAlert];
                                [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                                [self presentViewController:failureAlertController animated:YES completion:nil];
                            }];
                        }
                    }else{
                        [userWroungAttempt setInteger:savedLoginFailedAttemptValue forKey:[stloginUserId stringByAppendingString:@"failedLoginAttempt"]];
                        [userWroungAttempt synchronize];
                        [self dismissViewControllerAnimated:progressController completion:^{
                            UIAlertController *failureAlertController =nil;
                            failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Wrong User ID or PIN" preferredStyle:UIAlertControllerStyleAlert];
                            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                            [self presentViewController:failureAlertController animated:YES completion:nil];
                        }];
                    }
                }
            }else{
                savedLoginFailedAttemptValue = loginFailedCount+1;
                [userWroungAttempt setInteger:savedLoginFailedAttemptValue forKey:[stloginUserId stringByAppendingString:@"failedLoginAttempt"]];
                [userWroungAttempt synchronize];
                [self dismissViewControllerAnimated:progressController completion:^{
                    UIAlertController *failureAlertController =nil;
                    failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Wrong User ID or PIN" preferredStyle:UIAlertControllerStyleAlert];
                    [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                    [self presentViewController:failureAlertController animated:YES completion:nil];
                }];
            }
        }else{
            [self dismissViewControllerAnimated:progressController completion:^{
                UIAlertController *failureAlertController =nil;
                failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"User ID is not registered please register your user or try different User ID" preferredStyle:UIAlertControllerStyleAlert];
                [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:failureAlertController animated:YES completion:nil];
            }];
        }
    }
    
}

-(NSString*) resetUser{
    
    NSCharacterSet * queryKVSet = [NSCharacterSet
                                   characterSetWithCharactersInString:@"+"
                                   ].invertedSet;
    NSString * parameterString = [NSString                                  stringWithFormat:@"userid=%@&type=%d",
                                  _loginUserId.text,8
                                  ];
    NSLog(@"%@", parameterString);
    NSString* encodedUrl = [parameterString stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet];
    
    NSData *postData = [encodedUrl dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
    
    NSString *postLength=[NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    Details* details=[[Details alloc]init];
    NSString* urlDetails = [details getURL];
    NSString* post_url= [urlDetails stringByAppendingString:@"/ResetUser"];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@",post_url]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString* aStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] ;
    NSLog(@"responseData %@",aStr);
    return aStr;
}

@end
