//
//  ErrorViewController.h
//  AcledaPush
//
//  Created by Abhishek Ingle on 24/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ErrorViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *errorLabel;
- (IBAction)errorRedirect:(id)sender;
@property ( nonatomic, strong ) NSString *userId;
@end
