//
//  VerifyRegCodeController.m
//  AcledaPush
//
//  Created by Abhishek Ingle on 14/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import "VerifyRegCodeController.h"
#import "Details.h"
#import "SetMPINController.h"

@interface VerifyRegCodeController ()

@end

@implementation VerifyRegCodeController

static NSString *keyboardOpened=nil;
int otpTokenType=4;
int tokenCategoryVerifyController=4;
int tokenSubcategoryVerifyController=1;
int regCodeOnEmailVerifyController=4;
int regCodeOnSMSVerifyController=1;

+(NSString*)keyboardOpened{return  keyboardOpened;}
+(void)setKeyboardOpened:(NSString*)value{
    keyboardOpened=value;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //[_regCodeOne becomeFirstResponder];
    
    [_regCodeOne addTarget:self action:@selector(regCodeOneChanged) forControlEvents:UIControlEventEditingChanged];
    [_regCodeTwo addTarget:self action:@selector(regCodeTwoChanged) forControlEvents:UIControlEventEditingChanged];
    [_regCodeThree addTarget:self action:@selector(regCodeThreeChanged) forControlEvents:UIControlEventEditingChanged];
    [_regCodeFour addTarget:self action:@selector(regCodeFourChanged) forControlEvents:UIControlEventEditingChanged];
    [_regCodeFive addTarget:self action:@selector(regCodeFiveChanged) forControlEvents:UIControlEventEditingChanged];
    [_regCodeSix addTarget:self action:@selector(regCodeSixChanged) forControlEvents:UIControlEventEditingChanged];
    [_regCodeSeven addTarget:self action:@selector(regCodeSevenChanged) forControlEvents:UIControlEventEditingChanged];
    [_regCodeEight addTarget:self action:@selector(regCodeEightChanged) forControlEvents:UIControlEventEditingChanged];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)regCodeOneChanged{
    NSString* regCodeOne = _regCodeOne.text;
    if(![regCodeOne isEqualToString:@""]){
       // _regCodeOne.secureTextEntry = YES;
        [self nextRegCodeFocus1];
    }
}
- (void)regCodeTwoChanged{
    NSString* regCodeTwo = _regCodeTwo.text;
    if(![regCodeTwo isEqualToString:@""]){
        //_regCodeTwo.secureTextEntry = YES;
        [self nextRegCodeFocus2];
    }
}
- (void)regCodeThreeChanged{
    NSString* regCodeThree = _regCodeThree.text;
    if(![regCodeThree isEqualToString:@""]){
       // _regCodeThree.secureTextEntry = YES;
        [self nextRegCodeFocus3];
    }
}
- (void)regCodeFourChanged{
    NSString* regCodeFour = _regCodeFour.text;
    if(![regCodeFour isEqualToString:@""]){
       // _regCodeFour.secureTextEntry = YES;
        [self nextRegCodeFocus4];
    }
}

- (void)regCodeFiveChanged{
    NSString* regCodeFive = _regCodeFive.text;
    if(![regCodeFive isEqualToString:@""]){
        // _regCodeFive.secureTextEntry = YES;
        [self nextRegCodeFocus5];
    }
}
- (void)regCodeSixChanged{
    NSString* regCodeSix = _regCodeSix.text;
    if(![regCodeSix isEqualToString:@""]){
        // _regCodeSix.secureTextEntry = YES;
        [self nextRegCodeFocus6];
    }
}
- (void)regCodeSevenChanged{
    NSString* regCodeSeven = _regCodeSeven.text;
    if(![regCodeSeven isEqualToString:@""]){
        // _regCodeFour.secureTextEntry = YES;
        [self nextRegCodeFocus7];
    }
}
- (void)regCodeEightChanged{
    NSString* regCodeEight = _regCodeEight.text;
    if(![regCodeEight isEqualToString:@""]){
        // _regCodeFour.secureTextEntry = YES;
        [self nextRegCodeFocus8];
    }
}
-(void) nextRegCodeFocus1 {
    [_regCodeTwo becomeFirstResponder];
}

-(void) nextRegCodeFocus2 {
    [_regCodeThree becomeFirstResponder];
}

-(void) nextRegCodeFocus3 {
    [_regCodeFour becomeFirstResponder];
}

-(void) nextRegCodeFocus4 {
    [_regCodeFive becomeFirstResponder];
}
-(void) nextRegCodeFocus5 {
    [_regCodeSix becomeFirstResponder];
}
-(void) nextRegCodeFocus6 {
    [_regCodeSeven becomeFirstResponder];
}
-(void) nextRegCodeFocus7 {
    [_regCodeEight becomeFirstResponder];
}
-(void) nextRegCodeFocus8 {
    [_regCodeEight resignFirstResponder];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSString*) verifyRegCode{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *currentUserRegister = [prefs objectForKey:@"currentRegisterInUser"];
    NSLog(@"currentUserReg %@",currentUserRegister);
    NSString *stRegCode = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",_regCodeOne.text,_regCodeTwo.text,_regCodeThree.text,_regCodeFour.text,_regCodeFive.text,_regCodeSix.text,_regCodeSeven.text,_regCodeEight.text];
    NSCharacterSet * queryKVSet = [NSCharacterSet
                                   characterSetWithCharactersInString:@"+"
                                   ].invertedSet;
    NSString *uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString * parameterString = [NSString                                  stringWithFormat:@"userid=%@&regCode=%@&deviceid=%@&tokenType=%d",
                                  currentUserRegister,stRegCode,uniqueIdentifier,otpTokenType
                                  ];
    NSLog(@"%@", parameterString);
    NSString* encodedUrl = [parameterString stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet];
    
    NSData *postData = [encodedUrl dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
    
    NSString *postLength=[NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    Details* details=[[Details alloc]init];
    NSString* urlDetails = [details getURL];
    NSString* post_url= [urlDetails stringByAppendingString:@"/ValidateRegistrationCode"];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@",post_url]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString* aStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] ;
    NSLog(@"responseData %@",aStr);
    return aStr;
}

-(NSString*) activatePushAuth{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *currentUserRegister = [prefs objectForKey:@"currentRegisterInUser"];
    NSLog(@"currentUserReg %@",currentUserRegister);
    Details* details=[[Details alloc]init];
    NSString *deviceName = [details deviceModel];
    NSString *uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *strdata =[NSString stringWithFormat:@"{\"_devicetoken\":\"%@\",\"_devicetype\":\"%@\",\"_deviceId\":\"%@\",\"_deviceName\":\"%@\"}",Details.deviceToken,@"2",uniqueIdentifier,deviceName];
    NSLog(@"JSONString %@",strdata);
    NSCharacterSet * queryKVSet = [NSCharacterSet
                                   characterSetWithCharactersInString:@"+"
                                   ].invertedSet;
    
    NSString * parameterString = [NSString                                  stringWithFormat:@"userid=%@&regCode=%@&devicePayload=%@",
                                  _userId,_regCode.text,strdata
                                  ];
    NSLog(@"%@", parameterString);
    NSString* encodedUrl = [parameterString stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet];
    
    NSData *postData = [encodedUrl dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
    
    NSString *postLength=[NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    
    NSString* urlDetails = [details getURL];
    NSString* post_url= [urlDetails stringByAppendingString:@"/ActivatePushAuthentication"];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@",post_url]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString* aStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] ;
    NSLog(@"responseData %@",aStr);
    return aStr;
}

-(BOOL) textFieldShouldReturn: (UITextField *) textField
{
    [textField resignFirstResponder];
    if(keyboardOpened!=nil && [keyboardOpened isEqualToString:@"YES"]){
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
        keyboardOpened=nil;
    }
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField== _regCodeOne || textField== _regCodeTwo || textField== _regCodeThree || textField== _regCodeFour || textField== _regCodeFive || textField== _regCodeSix || textField== _regCodeSeven || textField== _regCodeEight){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        keyboardOpened=@"YES";
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    // This 'tabs' to next field when entering digits
    if (newLength == 1) {
        if (textField == _regCodeOne)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_regCodeTwo afterDelay:0.2];
        }
        else if (textField == _regCodeTwo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_regCodeThree afterDelay:0.2];
        }
        else if (textField == _regCodeThree)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_regCodeFour afterDelay:0.2];
        }
        else if (textField == _regCodeFour)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_regCodeFive afterDelay:0.2];
        }
        else if (textField == _regCodeFive)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_regCodeSix afterDelay:0.2];
        }
        else if (textField == _regCodeSix)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_regCodeSeven afterDelay:0.2];
        }
        else if (textField == _regCodeSeven)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_regCodeEight afterDelay:0.2];
        }
    }
    //this goes to previous field as you backspace through them, so you don't have to tap into them individually
    else if (oldLength > 0 && newLength == 0) {
        if (textField == _regCodeEight)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_regCodeSeven afterDelay:0.1];
        }
        else if (textField == _regCodeSeven)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_regCodeSix afterDelay:0.1];
        }
        else if (textField == _regCodeSix)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_regCodeFive afterDelay:0.1];
        }
        else if (textField == _regCodeFive)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_regCodeFour afterDelay:0.1];
        }
        else if (textField == _regCodeFour)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_regCodeThree afterDelay:0.1];
        }
        else if (textField == _regCodeThree)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_regCodeTwo afterDelay:0.1];
        }
        else if (textField == _regCodeTwo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_regCodeOne afterDelay:0.1];
        }
    }
    
    return newLength <= 1;
}

- (void)setNextResponder:(UITextField *)nextResponder
{
    [nextResponder becomeFirstResponder];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -keyboardSize.height;
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}

-(void) doVerificationOfRegCode{
    NSString *response = self.verifyRegCode;
    NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSString* strResult=[json objectForKey:@"result"];
    NSLog(@"strResult %@",strResult);
    if([strResult isEqualToString:@"success"]){
        NSString *responseForRegCodeSending = self.activatePushAuth;
        
        NSData *dataForEmail = [responseForRegCodeSending dataUsingEncoding:NSUTF8StringEncoding];
        id jsonForEmail = [NSJSONSerialization JSONObjectWithData:dataForEmail options:0 error:nil];
        NSString* strResultForEmail=[jsonForEmail objectForKey:@"result"];
        
        if([strResultForEmail isEqualToString:@"success"]){
            [self dismissViewControllerAnimated:progressControllerForVerifyReg completion:^{
                UIAlertController *failureAlertController =nil;
                failureAlertController = [UIAlertController alertControllerWithTitle:@"Success" message:@"Registration Code has been verified successfully" preferredStyle:UIAlertControllerStyleAlert];
                [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        SetMPINController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SetMpin"];
                        vc.userId=[_userId stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        [self presentViewController:vc animated:YES completion:nil];
                    });
                }]];
                [self presentViewController:failureAlertController animated:YES completion:nil];
            }];
        }else if ([strResultForEmail isEqualToString:@"error"]){
            [self dismissViewControllerAnimated:progressControllerForVerifyReg completion:^{
                UIAlertController *failureAlertController =nil;
                failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Unable to register your device" preferredStyle:UIAlertControllerStyleAlert];
                [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:failureAlertController animated:YES completion:nil];
            }];
        }else{
            [self dismissViewControllerAnimated:progressControllerForVerifyReg completion:^{
                UIAlertController *failureAlertController =nil;
                failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Unable to register your device" preferredStyle:UIAlertControllerStyleAlert];
                [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:failureAlertController animated:YES completion:nil];
            }];
        }
        
    }else if(strResult == nil){
        [self dismissViewControllerAnimated:progressControllerForVerifyReg completion:^{
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Server is not responding" preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
        }];
    }else{
        [self dismissViewControllerAnimated:progressControllerForVerifyReg completion:^{
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:[json objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
        }];
    }
}
UIAlertController *progressControllerForVerifyReg;
- (IBAction)verifyRegCode:(id)sender {    
    progressControllerForVerifyReg = [UIAlertController alertControllerWithTitle:@"Please wait" message:@"Verification of Reg Code is in process ..." preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:progressControllerForVerifyReg animated:YES completion:^{
        dispatch_async(dispatch_get_main_queue(), ^{[self doVerificationOfRegCode];});
    }];
}

-(NSString*) resendActivationCodeOnSMS{
    
    NSCharacterSet * queryKVSet = [NSCharacterSet
                                   characterSetWithCharactersInString:@"+"
                                   ].invertedSet;
    
    NSString * parameterString = [NSString                                  stringWithFormat:@"userid=%@&category=%d&subcategory=%d&type=%d",
                                  _userId,tokenCategoryVerifyController,tokenSubcategoryVerifyController,regCodeOnSMSVerifyController
                                  ];
    NSLog(@"%@", parameterString);
    NSString* encodedUrl = [parameterString stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet];
    
    NSData *postData = [encodedUrl dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
    
    NSString *postLength=[NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    Details* details = [[Details alloc]init];
    NSString* urlDetails = [details getURL];
    NSString* post_url= [urlDetails stringByAppendingString:@"/ResendActivationCode"];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@",post_url]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString* aStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] ;
    NSLog(@"responseData %@",aStr);
    return aStr;
}

-(NSString*) resendActivationCodeOnEmail{
    
    NSCharacterSet * queryKVSet = [NSCharacterSet
                                   characterSetWithCharactersInString:@"+"
                                   ].invertedSet;
    
    NSString * parameterString = [NSString                                  stringWithFormat:@"userid=%@&category=%d&subcategory=%d&type=%d",
                                  _userId,tokenCategoryVerifyController,tokenSubcategoryVerifyController,regCodeOnEmailVerifyController
                                  ];
    NSLog(@"%@", parameterString);
    NSString* encodedUrl = [parameterString stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet];
    
    NSData *postData = [encodedUrl dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
    
    NSString *postLength=[NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    Details* details = [[Details alloc]init];
    NSString* urlDetails = [details getURL];
    NSString* post_url= [urlDetails stringByAppendingString:@"/ResendActivationCode"];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@",post_url]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString* aStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] ;
    NSLog(@"responseData %@",aStr);
    return aStr;
}
-(void) doResendRegCodeOnEmail{
    NSLog(@"Resent Registration Code On Email");
    NSString *response = self.resendActivationCodeOnEmail;
    NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSString* strResult=[json objectForKey:@"result"];
    NSLog(@"strResult %@",strResult);
    if([strResult isEqualToString:@"success"]){
        [self dismissViewControllerAnimated:progressControllerForVerifyReg completion:^{
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"Success" message:@"Reg Code successfully sent on your email" preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
            }]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
        }];
    }else if ([strResult isEqualToString:@"error"]){
        [self dismissViewControllerAnimated:progressControllerForVerifyReg completion:^{
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Unable to sent reg code" preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
        }];
    }else if(strResult == nil){
        [self dismissViewControllerAnimated:progressControllerForVerifyReg completion:^{
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Server is not responding" preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
        }];
    }else{
        [self dismissViewControllerAnimated:progressControllerForVerifyReg completion:^{
        UIAlertController *failureAlertController =nil;
        failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:[json objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
        [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:failureAlertController animated:YES completion:nil];
        }];
    }
}
- (IBAction)resentRegCodeOnEmail:(id)sender {
    progressControllerForVerifyReg = [UIAlertController alertControllerWithTitle:@"Please wait" message:@"Sending Reg Code on your email is in process ..." preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:progressControllerForVerifyReg animated:YES completion:^{
        dispatch_async(dispatch_get_main_queue(), ^{[self doResendRegCodeOnEmail];});
    }];
}

-(void) doResendRegCodeOnSMS{
    NSLog(@"Resent Registration Code On SMS");
    NSString *response = self.resendActivationCodeOnEmail;
    NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSString* strResult=[json objectForKey:@"result"];
    NSLog(@"strResult %@",strResult);
    if([strResult isEqualToString:@"success"]){
        [self dismissViewControllerAnimated:progressControllerForVerifyReg completion:^{
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"Success" message:@"Reg Code successfully sent on your mobile" preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
            }]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
        }];
    }else if ([strResult isEqualToString:@"error"]){
       [self dismissViewControllerAnimated:progressControllerForVerifyReg completion:^{
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Unable to sent reg code" preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
       }];
    }else if(strResult == nil){
        [self dismissViewControllerAnimated:progressControllerForVerifyReg completion:^{
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Server is not responding" preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
        }];
    }else{
        [self dismissViewControllerAnimated:progressControllerForVerifyReg completion:^{
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:[json objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
        }];
    }
}
- (IBAction)resentRegCodeOnSMS:(id)sender {
    progressControllerForVerifyReg = [UIAlertController alertControllerWithTitle:@"Please wait" message:@"Sending Reg Code on your phone is in process ..." preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:progressControllerForVerifyReg animated:YES completion:^{
        dispatch_async(dispatch_get_main_queue(), ^{[self doResendRegCodeOnSMS];});
    }];
}
@end
