//
//  TransactionViewCell.h
//  AcledaPush
//
//  Created by Abhishek Ingle on 23/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionViewCell : UITableViewCell

@property(nonnull,strong) IBOutlet UILabel *titleLbl;
@property(nonnull,strong) IBOutlet UILabel *descLbl;

-(void) updateCellWithTitle:(nonnull NSString *)title description:(nonnull NSString*)desc;
@end
