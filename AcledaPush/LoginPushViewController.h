//
//  LoginPushViewController.h
//  AcledaPush
//
//  Created by Abhishek Ingle on 24/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginPushViewController : UIViewController
{
    NSTimer *countDownTimer;
    int secondsCount;
}
@property ( nonatomic, strong ) NSString *userId;
- (IBAction)denyLoginPush:(UIButton *)sender;
- (IBAction)approveLoginPush:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *transactionHeading;
@end
