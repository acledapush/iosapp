//
//  LoginPushViewController.m
//  AcledaPush
//
//  Created by Abhishek Ingle on 24/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import "LoginPushViewController.h"
#import "TransactionDetails.h"
#import "Details.h"
#import "SuccessViewController.h"
#import "ErrorViewController.h"

@interface LoginPushViewController ()<UITableViewDataSource, UITableViewDelegate>{
    
    NSMutableArray *loginTransactionDetails;
    
}
@property (weak, nonatomic) IBOutlet UITableView *loginTable;

@end

@implementation LoginPushViewController
NSString *pushtxId;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self arraySetUp];
    [self setTimer];
    
}
-(void) timerRun{
    secondsCount = secondsCount - 1;
    NSString *timerOutPut = [NSString stringWithFormat:@"%2d", secondsCount];
    cell.textLabel.text = loginTransactionDetails[5];
    cell.detailTextLabel.text = [timerOutPut stringByAppendingString:@" Seconds"];
    if(secondsCount <= 20){
        cell.detailTextLabel.textColor = [UIColor orangeColor];
    }
    if(secondsCount <= 10){
        cell.detailTextLabel.textColor = [UIColor redColor];
    }
    if(secondsCount ==0){
        [countDownTimer invalidate];
        countDownTimer = nil;
        [self denyLogin];
            dispatch_async(dispatch_get_main_queue(), ^{
                ErrorViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"errorController"];
                [self presentViewController:vc animated:YES completion:nil];
            });
        
        int i = self.writeInPushHistory;
        NSLog(@"histroy function %d",i);
    }
    
}
-(void) setTimer{
    TransactionDetails *txObj = [self returnPushData];
    NSString *expiryTime = txObj.transactionExpiryTime;
    expiryTime = [expiryTime stringByReplacingOccurrencesOfString:@"Seconds"  withString:@""];
    secondsCount = [expiryTime intValue];
    countDownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerRun) userInfo:nil repeats:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)arraySetUp {
    loginTransactionDetails = [NSMutableArray arrayWithArray:@[@"User ID",@"IP",@"Location",@"Date",@"Message",@"Expiry Time"]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return loginTransactionDetails.count;
}

UITableViewCell *cell;

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"lCell";
    TransactionDetails *txObj = [self returnPushData];
    cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    cell.textLabel.text = loginTransactionDetails[indexPath.row];
    int txRow = (int)indexPath.row + 1;
    if(txRow == 1){
        cell.detailTextLabel.text = _userId;
    }else if(txRow == 2){
        cell.detailTextLabel.text = txObj.transactionIp;
    }else if(txRow == 3){
        cell.detailTextLabel.text = txObj.transactionLocation;
    }else if(txRow == 4){
        cell.detailTextLabel.text = txObj.transactionDate;
    }else if(txRow == 5){
        cell.detailTextLabel.text = txObj.transactionMessage;
    }else if(txRow == 6){
        cell.detailTextLabel.text = txObj.transactionExpiryTime;
    }
    pushtxId=txObj.transactionID;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
-(NSString *) LoadFile:(NSString *)filename{
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *strFileName = [NSString stringWithFormat:@"%@/%@",
                             documentsDirectory,filename];
    NSString *content = [[NSString alloc] initWithContentsOfFile:strFileName
                                                    usedEncoding:nil
                                                           error:nil];
    return content;
    //use simple alert from my library (see previous post for details)
    // [ASFunctions alert:content];
    //  [content release];
    
}

-(TransactionDetails *) returnPushData{
    
    NSString* txIP = @"NA";
    NSString* txAction = @"NA";
    NSString* txMessage = @"NA";
    NSString* txDate = @"NA";
    NSString* txExpiry = @"NA";
    NSString* txID = @"NA";
    NSString* txLocation = @"NA";
    
    TransactionDetails *transactionDetails = [[TransactionDetails alloc]init];
    NSString *strPreviousJsonEncData = [self LoadFile:[@"livePushDetails" stringByAppendingString:_userId]];
    
    NSLog(@"previousJSON %@",strPreviousJsonEncData);
    
    if(strPreviousJsonEncData != nil){
        
        NSData *data = [strPreviousJsonEncData dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"pushData %@", [json objectForKey:@"pushData"]);
        NSString *strPushData = [json objectForKey:@"pushData"];
        NSData *pushdata = [strPushData dataUsingEncoding:NSUTF8StringEncoding];
        id pushDatajson = [NSJSONSerialization JSONObjectWithData:pushdata options:0 error:nil];
        NSLog(@"pushDatajson %@", pushDatajson);
        
        txIP = [pushDatajson objectForKey:@"IP"];
        txAction = [pushDatajson objectForKey:@"Action"];
        txDate = [pushDatajson objectForKey:@"Date"];
        txMessage = [pushDatajson objectForKey:@"Message"];
        txExpiry = [pushDatajson objectForKey:@"Expiry Time"];
        txID = [pushDatajson objectForKey:@"pushTxId"];
        txLocation = [pushDatajson objectForKey:@"Location"];
    }
    // set transaction details
    [transactionDetails setTransactionIp:txIP];
    [transactionDetails setTransactionAction:txAction];
    [transactionDetails setTransactionDate:txDate];
    [transactionDetails setTransactionMessage:txMessage];
    [transactionDetails setTransactionExpiryTime:txExpiry];
    [transactionDetails setTransactionID:txID];
    [transactionDetails setTransactionLocation:txLocation];
    return transactionDetails;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
UIAlertController *progressControllerForTx =nil;

- (IBAction)denyLoginPush:(UIButton *)sender {
    [countDownTimer invalidate];
    countDownTimer = nil;
//    UIAlertController *gpsAlertController =nil;
//    gpsAlertController = [UIAlertController alertControllerWithTitle:@"Are You Sure" message:@"" preferredStyle:UIAlertControllerStyleAlert];
//    [gpsAlertController addAction:[UIAlertAction actionWithTitle:@"Confirm" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        progressControllerForTx = [UIAlertController alertControllerWithTitle:@"Please Wait" message:@"Your request is in progress ..." preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:progressControllerForTx animated:YES completion:^{
            [self doDenyPushTransaction];
        }];
//    }]];
//    [gpsAlertController addAction:[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//        dispatch_async(dispatch_get_main_queue(), ^{
//        });
//    }]];
//    [self presentViewController:gpsAlertController animated:YES completion:nil];
}


-(void) doDenyPushTransaction{
    NSString *response = self.denyLogin;
    NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSString* strResult=[json objectForKey:@"result"];
    NSString* strResultCode=[json objectForKey:@"resultCode"];
    NSLog(@"strResult %@",strResult);
    if(strResult == nil){
        [self dismissViewControllerAnimated:progressControllerForTx completion:^{
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Server is not responding" preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
        }];
    }else if([strResult isEqualToString:@"success"]){
        // push history
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *currentUserLogin = [prefs objectForKey:@"currentLogiedInUser"];
        NSString *stroedPreviousHistoryData = [self LoadFile:[@"historyPushDetails" stringByAppendingString:currentUserLogin]];
        NSString *strTxOneData = nil;
        NSString *strTxTwoData = nil;
        NSString *strTxThreeData = nil;
        NSString *strTxFourData = nil;
        NSString *strTxFiveData = nil;
        if(stroedPreviousHistoryData != nil){
            NSData *data = [stroedPreviousHistoryData dataUsingEncoding:NSUTF8StringEncoding];
            id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            strTxOneData = [json objectForKey:@"TxOne"];
            strTxTwoData = [json objectForKey:@"TxTwo"];
            strTxThreeData = [json objectForKey:@"TxThree"];
            strTxFourData = [json objectForKey:@"TxFour"];
            strTxFiveData = [json objectForKey:@"TxFive"];
        }
        //current Push
        NSString *stroedLivePushData = [self LoadFile:[@"livePushDetails" stringByAppendingString:currentUserLogin]];
        NSString *txActionDetail = @"NA";
        if(stroedLivePushData != nil){
            
            // Get Live Push Details
            
            NSData *livePushData = [stroedLivePushData dataUsingEncoding:NSUTF8StringEncoding];
            id livePushJson = [NSJSONSerialization JSONObjectWithData:livePushData options:0 error:nil];
            NSString *TxOnePushData = [livePushJson objectForKey:@"pushData"];
            NSData *txOnePushData = [TxOnePushData dataUsingEncoding:NSUTF8StringEncoding];
            id livetxOnePushDatajson = [NSJSONSerialization JSONObjectWithData:txOnePushData options:0 error:nil];
            txActionDetail = [livetxOnePushDatajson objectForKey:@"Action"];
            
            NSString *struserid = [livePushJson objectForKey:@"userid"];
            NSString *strPushData = [livePushJson objectForKey:@"pushData"];
            NSString *strapsData = [livePushJson objectForKey:@"apsData"];
            NSString *strcurrentDate = [livePushJson objectForKey:@"currentDate"];
            NSString* status=@"2";
            
            NSMutableDictionary *pStore = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           struserid,@"userid",
                                           strPushData,@"pushData",
                                           strapsData ,@"apsData",
                                           strcurrentDate,@"currentDate",
                                           status,@"status",
                                           nil
                                           ];
            NSString *strJsonEncData = [self parseJSON:pStore];
            
            NSMutableDictionary *storePushHistory = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                     strJsonEncData,@"TxOne",
                                                     strTxOneData,@"TxTwo",
                                                     strTxTwoData,@"TxThree",
                                                     strTxThreeData,@"TxFour",
                                                     strTxFourData,@"TxFive",
                                                     nil
                                                     ];
            NSString *strJsonEncHistoryData = [self parseJSON:storePushHistory];
            int iResult = [self WriteFile:[@"historyPushDetails" stringByAppendingString:struserid] usingWith:strJsonEncHistoryData];
            NSLog(@"File Write Result >> %d",iResult);
        }
        [self dismissViewControllerAnimated:progressControllerForTx completion:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                ErrorViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"errorController"];
                
                [self presentViewController:vc animated:YES completion:nil];
                if([txActionDetail isEqualToString:@"L"]){
                    vc.errorLabel.text=@"You have denied your Login Request";
                    
                }else{
                    vc.errorLabel.text=@"You have denied your Transaction Request";
                }
                vc.errorLabel.textColor=[UIColor redColor];
            });
        }];
    } else if ([strResult isEqualToString:@"error"]){
        if([strResultCode isEqualToString:@"-269"])
        {
            [self dismissViewControllerAnimated:progressControllerForTx completion:^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    ErrorViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"errorController"];
                    
                    [self presentViewController:vc animated:YES completion:nil];
                    //                    vc.successMessage.text=@"Your transaction request is expired";
                });
            }];
            int i = self.writeInPushHistory;
            NSLog(@"histroy function %d",i);
        }else{
            [self dismissViewControllerAnimated:progressControllerForTx completion:^{
                UIAlertController *failureAlertController =nil;
                failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:[json objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:failureAlertController animated:YES completion:nil];
            }];
        }
    }
}

-(void) doApprovePushTransaction{
    
    NSString *response = self.approveLogin;
    NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSString* strResult=[json objectForKey:@"result"];
    NSString* strResultCode=[json objectForKey:@"resultCode"];
    NSLog(@"strResult %@",strResult);
    if(strResult == nil){
        [self dismissViewControllerAnimated:progressControllerForTx completion:^{
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Server is not responding" preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
        }];
    }else if([strResult isEqualToString:@"success"]){
        // push history
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *currentUserLogin = [prefs objectForKey:@"currentLogiedInUser"];
        NSString *stroedPreviousHistoryData = [self LoadFile:[@"historyPushDetails" stringByAppendingString:currentUserLogin]];
        NSString *strTxOneData = nil;
        NSString *strTxTwoData = nil;
        NSString *strTxThreeData = nil;
        NSString *strTxFourData = nil;
        NSString *strTxFiveData = nil;
        if(stroedPreviousHistoryData != nil){
            NSData *data = [stroedPreviousHistoryData dataUsingEncoding:NSUTF8StringEncoding];
            id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            strTxOneData = [json objectForKey:@"TxOne"];
            strTxTwoData = [json objectForKey:@"TxTwo"];
            strTxThreeData = [json objectForKey:@"TxThree"];
            strTxFourData = [json objectForKey:@"TxFour"];
            strTxFiveData = [json objectForKey:@"TxFive"];
        }
        //current Push
        NSString *stroedLivePushData = [self LoadFile:[@"livePushDetails" stringByAppendingString:currentUserLogin]];
        NSString *txActionDetail = @"NA";
        if(stroedLivePushData != nil){
            NSData *pushdata = [stroedLivePushData dataUsingEncoding:NSUTF8StringEncoding];
            id pushDatajson = [NSJSONSerialization JSONObjectWithData:pushdata options:0 error:nil];
            NSString *TxOnePushData = [pushDatajson objectForKey:@"pushData"];
            NSData *txOnePushData = [TxOnePushData dataUsingEncoding:NSUTF8StringEncoding];
            id livetxOnePushDatajson = [NSJSONSerialization JSONObjectWithData:txOnePushData options:0 error:nil];
            
             txActionDetail = [livetxOnePushDatajson objectForKey:@"Action"];
            // Get Live Push Details
            NSData *livePushData = [stroedLivePushData dataUsingEncoding:NSUTF8StringEncoding];
            id livePushJson = [NSJSONSerialization JSONObjectWithData:livePushData options:0 error:nil];
            
            NSString *struserid = [livePushJson objectForKey:@"userid"];
            NSString *strPushData = [livePushJson objectForKey:@"pushData"];
            NSString *strapsData = [livePushJson objectForKey:@"apsData"];
            NSString *strcurrentDate = [livePushJson objectForKey:@"currentDate"];
            NSString* status=@"1";
            
            NSMutableDictionary *pStore = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           struserid,@"userid",
                                           strPushData,@"pushData",
                                           strapsData ,@"apsData",
                                           strcurrentDate,@"currentDate",
                                           status,@"status",
                                           nil
                                           ];
            NSString *strJsonEncData = [self parseJSON:pStore];
            
            NSMutableDictionary *storePushHistory = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                     strJsonEncData,@"TxOne",
                                                     strTxOneData,@"TxTwo",
                                                     strTxTwoData,@"TxThree",
                                                     strTxThreeData,@"TxFour",
                                                     strTxFourData,@"TxFive",
                                                     nil
                                                     ];
            NSString *strJsonEncHistoryData = [self parseJSON:storePushHistory];
            int iResult = [self WriteFile:[@"historyPushDetails" stringByAppendingString:struserid] usingWith:strJsonEncHistoryData];
            NSLog(@"File Write Result >> %d",iResult);
        }
        [self dismissViewControllerAnimated:progressControllerForTx completion:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                SuccessViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"successController"];
                
                [self presentViewController:vc animated:YES completion:nil];
                 if([txActionDetail isEqualToString:@"L"]){
                     vc.successMessage.text=@"You have approved your Login Request";
                 }else{
                     vc.successMessage.text=@"You have approved your Transaction Request";
                 }
            });
        }];
    } else if ([strResult isEqualToString:@"error"]){
        if([strResultCode isEqualToString:@"-269"]){
            [self dismissViewControllerAnimated:progressControllerForTx completion:^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    ErrorViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"errorController"];
                    
                    [self presentViewController:vc animated:YES completion:nil];
//                    vc.successMessage.text=@"Your transaction request is expired";
                });
            }];
            int i = self.writeInPushHistory;
            NSLog(@"histroy function %d",i);
        }else{
            [self dismissViewControllerAnimated:progressControllerForTx completion:^{
                UIAlertController *failureAlertController =nil;
                failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:[json objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:failureAlertController animated:YES completion:nil];
            }];
        }
    }
}

- (IBAction)approveLoginPush:(UIButton *)sender {
    [countDownTimer invalidate];
    countDownTimer = nil;
//    UIAlertController *gpsAlertController =nil;
//
//    gpsAlertController = [UIAlertController alertControllerWithTitle:@"Are You Sure" message:@"" preferredStyle:UIAlertControllerStyleAlert];
//    [gpsAlertController addAction:[UIAlertAction actionWithTitle:@"Confirm" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        progressControllerForTx = [UIAlertController alertControllerWithTitle:@"Please Wait" message:@"Your request is in progress ..." preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:progressControllerForTx animated:YES completion:^{
            [self doApprovePushTransaction];
        }];
//    }]];
//    [gpsAlertController addAction:[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//        dispatch_async(dispatch_get_main_queue(), ^{
//        });
//    }]];
    //[self presentViewController:gpsAlertController animated:YES completion:nil];
}

-(int )writeInPushHistory{
    // push history
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *currentUserLogin = [prefs objectForKey:@"currentLogiedInUser"];
    NSString *stroedPreviousHistoryData = [self LoadFile:[@"historyPushDetails" stringByAppendingString:currentUserLogin]];
    NSString *strTxOneData = nil;
    NSString *strTxTwoData = nil;
    NSString *strTxThreeData = nil;
    NSString *strTxFourData = nil;
    NSString *strTxFiveData = nil;
    if(stroedPreviousHistoryData != nil){
        NSData *data = [stroedPreviousHistoryData dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        strTxOneData = [json objectForKey:@"TxOne"];
        strTxTwoData = [json objectForKey:@"TxTwo"];
        strTxThreeData = [json objectForKey:@"TxThree"];
        strTxFourData = [json objectForKey:@"TxFour"];
        strTxFiveData = [json objectForKey:@"TxFive"];
    }
    //current Push
    NSString *stroedLivePushData = [self LoadFile:[@"livePushDetails" stringByAppendingString:currentUserLogin]];
    
    if(stroedLivePushData != nil){
        
        // Get Live Push Details
        NSData *livePushData = [stroedLivePushData dataUsingEncoding:NSUTF8StringEncoding];
        id livePushJson = [NSJSONSerialization JSONObjectWithData:livePushData options:0 error:nil];
        
        NSString *struserid = [livePushJson objectForKey:@"userid"];
        NSString *strPushData = [livePushJson objectForKey:@"pushData"];
        NSString *strapsData = [livePushJson objectForKey:@"apsData"];
        NSString *strcurrentDate = [livePushJson objectForKey:@"currentDate"];
        NSString* status=@"-1";
        
        NSMutableDictionary *pStore = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       struserid,@"userid",
                                       strPushData,@"pushData",
                                       strapsData ,@"apsData",
                                       strcurrentDate,@"currentDate",
                                       status,@"status",
                                       nil
                                       ];
        NSString *strJsonEncData = [self parseJSON:pStore];
        
        NSMutableDictionary *storePushHistory = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                 strJsonEncData,@"TxOne",
                                                 strTxOneData,@"TxTwo",
                                                 strTxTwoData,@"TxThree",
                                                 strTxThreeData,@"TxFour",
                                                 strTxFourData,@"TxFive",
                                                 nil
                                                 ];
        NSString *strJsonEncHistoryData = [self parseJSON:storePushHistory];
        int iResult = [self WriteFile:[@"historyPushDetails" stringByAppendingString:struserid] usingWith:strJsonEncHistoryData];
        NSLog(@"File Write Result >> %d",iResult);
        return iResult;
    }
    return -1;
}
-(int) WriteFile:(NSString* )filename usingWith:(NSString* )filedata{
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/%@",
                          documentsDirectory,filename];
    //create content - four lines of text
    // NSString *content = @"One\nTwo\nThree\nFour\nFive";
    
    NSString *content = filedata;
    //save content to the documents directory
    bool result = [content writeToFile:fileName
                            atomically:NO
                              encoding:NSStringEncodingConversionAllowLossy
                                 error:nil];
    if(result == true){
        return 0;
    }else{
        return -1;
    }
    
}

-(NSString *)parseJSON:(NSMutableDictionary* )dictonary{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictonary options:NSJSONWritingPrettyPrinted error:&error];
    NSString *resultAsString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return resultAsString;
}

-(NSString*) approveLogin{
    NSLog(@"%@",pushtxId);
    NSString *uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *strdata =[NSString stringWithFormat:@"{\"transactionResponse\":\"%d\",\"_deviceid\":\"%@\"}",3,uniqueIdentifier];
    NSCharacterSet * queryKVSet = [NSCharacterSet
                                   characterSetWithCharactersInString:@"+"
                                   ].invertedSet;
    
    NSString * parameterString = [NSString                                  stringWithFormat:@"transactionId=%@&devicepayload=%@",
                                  pushtxId,strdata
                                  ];
    NSLog(@"%@", parameterString);
    NSString* encodedUrl = [parameterString stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet];
    
    NSData *postData = [encodedUrl dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
    
    NSString *postLength=[NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    Details* details = [[Details alloc]init];
    NSString* urlDetails = [details getURL];
    NSString* post_url= [urlDetails stringByAppendingString:@"/UpdateTransactionStatus"];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@",post_url]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString* aStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] ;
    NSLog(@"responseData %@",aStr);
    return aStr;
}

-(NSString*) denyLogin{
    NSLog(@"%@",pushtxId);
    NSString *uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *strdata =[NSString stringWithFormat:@"{\"transactionResponse\":\"%d\",\"_deviceid\":\"%@\"}",4,uniqueIdentifier];
    NSCharacterSet * queryKVSet = [NSCharacterSet
                                   characterSetWithCharactersInString:@"+"
                                   ].invertedSet;
    
    NSString * parameterString = [NSString                                  stringWithFormat:@"transactionId=%@&devicepayload=%@",
                                  pushtxId,strdata
                                  ];
    NSLog(@"%@", parameterString);
    NSString* encodedUrl = [parameterString stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet];
    
    NSData *postData = [encodedUrl dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
    
    NSString *postLength=[NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    Details* details = [[Details alloc]init];
    NSString* urlDetails = [details getURL];
    NSString* post_url= [urlDetails stringByAppendingString:@"/UpdateTransactionStatus"];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@",post_url]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString* aStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] ;
    NSLog(@"responseData %@",aStr);
    return aStr;
}
@end
