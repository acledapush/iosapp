//
//  ChangePinViewController.m
//  AcledaPush
//
//  Created by Abhishek Ingle on 25/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import "ChangePinViewController.h"
#import "SWRevealViewController.h"

@interface ChangePinViewController ()

@end

@implementation ChangePinViewController

static NSString *keyboardOpened=nil;

+(NSString*)keyboardOpened{return  keyboardOpened;}
+(void)setKeyboardOpened:(NSString*)value{
    keyboardOpened=value;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _barButton.target = self.revealViewController;
    _barButton.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    _currentPinOne.secureTextEntry = NO;
    _currentPinTwo.secureTextEntry = NO;
    _currentPinThree.secureTextEntry = NO;
    _currentPinFour.secureTextEntry = NO;
    
    _nPINOne.secureTextEntry = NO;
    _nPINTwo.secureTextEntry = NO;
    _nPINThree.secureTextEntry = NO;
    _nPINFour.secureTextEntry = NO;
    
    _confirmPINOne.secureTextEntry = NO;
    _confirmPINTwo.secureTextEntry = NO;
    _confirmPINThree.secureTextEntry = NO;
    _confirmPINFour.secureTextEntry = NO;
    
    //[_currentPinOne becomeFirstResponder];
    
    [_currentPinOne addTarget:self action:@selector(currentPinOneChanged) forControlEvents:UIControlEventEditingChanged];
    [_currentPinTwo addTarget:self action:@selector(currentPinTwoChanged2) forControlEvents:UIControlEventEditingChanged];
    [_currentPinThree addTarget:self action:@selector(currentPinThreeChanged3) forControlEvents:UIControlEventEditingChanged];
    [_currentPinFour addTarget:self action:@selector(currentPinFourChanged4) forControlEvents:UIControlEventEditingChanged];
    
    [_nPINOne addTarget:self action:@selector(nPINOneChanged) forControlEvents:UIControlEventEditingChanged];
    [_nPINTwo addTarget:self action:@selector(nPINTwoChanged2) forControlEvents:UIControlEventEditingChanged];
    [_nPINThree addTarget:self action:@selector(nPINThreeChanged3) forControlEvents:UIControlEventEditingChanged];
    [_nPINFour addTarget:self action:@selector(nPINFourChanged4) forControlEvents:UIControlEventEditingChanged];
  
    [_confirmPINOne addTarget:self action:@selector(confirmPINOneChanged) forControlEvents:UIControlEventEditingChanged];
    [_confirmPINTwo addTarget:self action:@selector(confirmPINTwoChanged2) forControlEvents:UIControlEventEditingChanged];
    [_confirmPINThree addTarget:self action:@selector(confirmPINThreeChanged3) forControlEvents:UIControlEventEditingChanged];
    [_confirmPINFour addTarget:self action:@selector(confirmPINFourChanged4) forControlEvents:UIControlEventEditingChanged];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //    [txtPasscode resignFirstResponder];
    
}

- (void)currentPinFourChanged4{
    NSString* pinFour = _currentPinFour.text;
    if(![pinFour isEqualToString:@""]){
        _currentPinFour.secureTextEntry = YES;
        [self nextCurrentPinFocus4];
    }
}
- (void)currentPinThreeChanged3{
    NSString* pinThree = _currentPinThree.text;
    if(![pinThree isEqualToString:@""]){
        _currentPinThree.secureTextEntry = YES;
        [self nextCurrentPinFocus3];
    }
}
- (void)currentPinTwoChanged2{
    NSString* pinTwo = _currentPinTwo.text;
    if(![pinTwo isEqualToString:@""]){
        _currentPinTwo.secureTextEntry = YES;
        [self nextCurrentPinFocus2];
    }
}
- (void)currentPinOneChanged{
    NSString* pinOne = _currentPinOne.text;
    if(![pinOne isEqualToString:@""]){
        _currentPinOne.secureTextEntry = YES;
        [self nextCurrentPinFocus1];
    }
}

-(void) nextCurrentPinFocus1 {
    [_currentPinTwo becomeFirstResponder];
}

-(void) nextCurrentPinFocus2 {
    [_currentPinThree becomeFirstResponder];
}

-(void) nextCurrentPinFocus3 {
    [_currentPinFour becomeFirstResponder];
}
-(void) nextCurrentPinFocus4 {
    [_currentPinFour resignFirstResponder];
}


- (void)nPINFourChanged4{
    NSString* pinFour = _nPINFour.text;
    if(![pinFour isEqualToString:@""]){
        _nPINFour.secureTextEntry = YES;
        [self nextNewPinFocus4];
    }
}
- (void)nPINThreeChanged3{
    NSString* pinThree = _nPINThree.text;
    if(![pinThree isEqualToString:@""]){
        _nPINThree.secureTextEntry = YES;
        [self nextNewPinFocus3];
    }
}
- (void)nPINTwoChanged2{
    NSString* pinTwo = _nPINTwo.text;
    if(![pinTwo isEqualToString:@""]){
        _nPINTwo.secureTextEntry = YES;
        [self nextNewPinFocus2];
    }
}
- (void)nPINOneChanged{
    NSString* pinOne = _nPINOne.text;
    if(![pinOne isEqualToString:@""]){
        _nPINOne.secureTextEntry = YES;
        [self nextNewPinFocus1];
    }
}

-(void) nextNewPinFocus1 {
    [_nPINTwo becomeFirstResponder];
}

-(void) nextNewPinFocus2 {
    [_nPINThree becomeFirstResponder];
}

-(void) nextNewPinFocus3 {
    [_nPINFour becomeFirstResponder];
}
-(void) nextNewPinFocus4 {
    [_nPINFour resignFirstResponder];
}

- (void)confirmPINFourChanged4{
    NSString* pinFour = _confirmPINFour.text;
    if(![pinFour isEqualToString:@""]){
        _confirmPINFour.secureTextEntry = YES;
        [self nextConfirmPinFocus4];
    }
}
- (void)confirmPINThreeChanged3{
    NSString* pinThree = _confirmPINThree.text;
    if(![pinThree isEqualToString:@""]){
        _confirmPINThree.secureTextEntry = YES;
        [self nextConfirmPinFocus3];
    }
}
- (void)confirmPINTwoChanged2{
    NSString* pinTwo = _confirmPINTwo.text;
    if(![pinTwo isEqualToString:@""]){
        _confirmPINTwo.secureTextEntry = YES;
        [self nextConfirmPinFocus2];
    }
}
- (void)confirmPINOneChanged{
    NSString* pinOne = _confirmPINOne.text;
    if(![pinOne isEqualToString:@""]){
        _confirmPINOne.secureTextEntry = YES;
        [self nextConfirmPinFocus1];
    }
}

-(void) nextConfirmPinFocus1 {
    [_confirmPINTwo becomeFirstResponder];
}

-(void) nextConfirmPinFocus2 {
    [_confirmPINThree becomeFirstResponder];
}

-(void) nextConfirmPinFocus3 {
    [_confirmPINFour becomeFirstResponder];
}
-(void) nextConfirmPinFocus4 {
    [_confirmPINFour resignFirstResponder];
}

// This allows numeric text only, but also backspace for deletes
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField == _userIdForChangePin){
        NSRange lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
        
        if (lowercaseCharRange.location != NSNotFound) {
            
            if (textField.text.length == 0) {
                
                // Add first character -- and update Return button
                //
                // But this has the effect of losing the editing point
                // (only when trying to edit with lowercase characters),
                // because the text of the UITextField is modified,
                // which does not matter since the caret is at the end
                // of the (empty) text fiueld anyways.
                //
                textField.text = [textField.text stringByReplacingCharactersInRange:range withString:[string uppercaseString]];
                
            } else {
                
                // Replace range instead of whole string in order
                // not to lose input caret position -- but this will
                // not update the Return button, which is not necessary
                // here anyways
                //
                UITextPosition *beginning = textField.beginningOfDocument;
                UITextPosition *start = [textField positionFromPosition:beginning offset:range.location];
                UITextPosition *end = [textField positionFromPosition:start offset:range.length];
                UITextRange *textRange = [textField textRangeFromPosition:start toPosition:end];
                
                [textField replaceRange:textRange withText:[string uppercaseString]];
            }
            
            return NO;
        }
        return YES;
    }
    
    
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    // This 'tabs' to next field when entering digits
    if (newLength == 1) {
        if (textField == _currentPinOne)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_currentPinTwo afterDelay:0.2];
        }
        else if (textField == _currentPinTwo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_currentPinThree afterDelay:0.2];
        }
        else if (textField == _currentPinThree)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_currentPinFour afterDelay:0.2];
        }
        else if (textField == _nPINOne)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_nPINTwo afterDelay:0.2];
        }
        else if (textField == _nPINTwo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_nPINThree afterDelay:0.2];
        }
        else if (textField == _nPINThree)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_nPINFour afterDelay:0.2];
        }
        
        else if (textField == _confirmPINOne)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_confirmPINTwo afterDelay:0.2];
        }
        else if (textField == _confirmPINTwo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_confirmPINThree afterDelay:0.2];
        }
        else if (textField == _confirmPINThree)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_confirmPINFour afterDelay:0.2];
        }
    }
    //this goes to previous field as you backspace through them, so you don't have to tap into them individually
    else if (oldLength > 0 && newLength == 0) {
        if (textField == _currentPinFour)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_currentPinThree afterDelay:0.1];
        }
        else if (textField == _currentPinThree)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_currentPinTwo afterDelay:0.1];
        }
        else if (textField == _currentPinTwo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_currentPinOne afterDelay:0.1];
        }
        else if (textField == _nPINFour)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_nPINThree afterDelay:0.1];
        }
        else if (textField == _nPINThree)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_nPINTwo afterDelay:0.1];
        }
        else if (textField == _nPINTwo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_nPINOne afterDelay:0.1];
        }
        
        else if (textField == _confirmPINFour)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_confirmPINThree afterDelay:0.1];
        }
        else if (textField == _confirmPINThree)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_confirmPINTwo afterDelay:0.1];
        }
        else if (textField == _confirmPINTwo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_confirmPINOne afterDelay:0.1];
        }
    }
    
    return newLength <= 1;
}

- (void)setNextResponder:(UITextField *)nextResponder
{
    [nextResponder becomeFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelChnageMPIN:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        SWRevealViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"homeScreen"];
        [self presentViewController:vc animated:YES completion:nil];
    });
}

- (IBAction)saveMPIN:(id)sender {
    UIAlertController *progressControllerForChangePin = [UIAlertController alertControllerWithTitle:@"Please wait" message:@"PIN change is in process ..." preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:progressControllerForChangePin animated:YES completion:nil];
    
    //NSString *stCurrentPIN = _currentPIN.text;
    NSString *stCurrentPIN = [NSString stringWithFormat:@"%@%@%@%@",_currentPinOne.text,_currentPinTwo.text,_currentPinThree.text,_currentPinFour.text];
    //NSString *stConfirmAuthPIN = _confirmAuthMPIN1.text;
    NSString *stConfirmAuthPIN = [NSString stringWithFormat:@"%@%@%@%@",_nPINOne.text,_nPINTwo.text,_nPINThree.text,_nPINFour.text];
    //NSString *stConfirmAuth = _confirmAuthMPIN.text;
    NSString *stConfirmAuth = [NSString stringWithFormat:@"%@%@%@%@",_confirmPINOne.text,_confirmPINTwo.text,_confirmPINThree.text,_confirmPINFour.text];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *currentUserLogin = [prefs objectForKey:@"currentLogiedInUser"];
    NSUserDefaults *pinDetails = [NSUserDefaults standardUserDefaults];
    NSString *savedValue = [pinDetails objectForKey:currentUserLogin];
    if([savedValue isEqualToString:stCurrentPIN]){
        if([stConfirmAuthPIN isEqualToString:stConfirmAuth]){
            [pinDetails setObject:stConfirmAuthPIN forKey:currentUserLogin];
            NSLog(@"chnageMPIN %@",[prefs stringForKey:currentUserLogin]);
            [prefs synchronize];
            [self dismissViewControllerAnimated:progressControllerForChangePin completion:^{
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"Success" message:@"Your PIN changed is successfully" preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
            }];
        }else{
            [self dismissViewControllerAnimated:progressControllerForChangePin completion:^{
                UIAlertController *failureAlertController =nil;
                failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"New PIN does not match with Confirm PIN" preferredStyle:UIAlertControllerStyleAlert];
                [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:failureAlertController animated:YES completion:nil];
            }];
        }
    }else{
       [self dismissViewControllerAnimated:progressControllerForChangePin completion:^{
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Current PIN does not match" preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
       }];
    }
    
}

-(BOOL) textFieldShouldReturn:(UITextField *) textField
{
    [textField resignFirstResponder];
    if(keyboardOpened!=nil && [keyboardOpened isEqualToString:@"YES"]){
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
        keyboardOpened=nil;
    }
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField==_confirmAuthMPIN1 || textField==_confirmAuthMPIN){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        keyboardOpened=@"YES";
    }
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
}



#pragma mark - keyboard movements
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -keyboardSize.height;
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}
@end
