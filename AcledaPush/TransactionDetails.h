//
//  TransactionDetails.h
//  AcledaPush
//
//  Created by Abhishek Ingle on 22/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransactionDetails : NSObject

@property NSString* transactionIp;
@property NSString* transactionDate;
@property NSString* transactionTime;
@property NSString* transactionExpiryTime;
@property NSString* transactionMessage;
@property NSString* transactionBrowser;
@property NSString* transactionOS;
@property NSString* transactionID;
@property NSString* transactionAction;
@property NSString* transactionStatus;
@property NSString* transactionLocation;


@property NSString* transactionIpTwo;
@property NSString* transactionDateTwo;
@property NSString* transactionTimeTwo;
@property NSString* transactionExpiryTimeTwo;
@property NSString* transactionMessageTwo;
@property NSString* transactionBrowserTwo;
@property NSString* transactionOSTwo;
@property NSString* transactionIDTwo;
@property NSString* transactionActionTwo;
@property NSString* transactionStatusTwo;
@property NSString* transactionLocationTwo;

@property NSString* transactionIpThree;
@property NSString* transactionDateThree;
@property NSString* transactionTimeThree;
@property NSString* transactionExpiryTimeThree;
@property NSString* transactionMessageThree;
@property NSString* transactionBrowserThree;
@property NSString* transactionOSThree;
@property NSString* transactionIDThree;
@property NSString* transactionActionThree;
@property NSString* transactionStatusThree;
@property NSString* transactionLocationThree;

@property NSString* transactionIpFour;
@property NSString* transactionDateFour;
@property NSString* transactionTimeFour;
@property NSString* transactionExpiryTimeFour;
@property NSString* transactionMessageFour;
@property NSString* transactionBrowserFour;
@property NSString* transactionOSFour;
@property NSString* transactionIDFour;
@property NSString* transactionActionFour;
@property NSString* transactionStatusFour;
@property NSString* transactionLocationFour;

@property NSString* transactionIpFive;
@property NSString* transactionDateFive;
@property NSString* transactionTimeFive;
@property NSString* transactionExpiryTimeFive;
@property NSString* transactionMessageFive;
@property NSString* transactionBrowserFive;
@property NSString* transactionOSFive;
@property NSString* transactionIDFive;
@property NSString* transactionActionFive;
@property NSString* transactionStatusFive;
@property NSString* transactionLocationFive;
@end
