//
//  AppDelegate.m
//  Notifications10ObjC
//
//  Created by SYS004 on 9/24/16.
//  Copyright © 2016 Kode. All rights reserved.
//

#import "AppDelegate.h"
#import "Details.h"
#import "LoginController.h"

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
// define macro
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [self registerForRemoteNotification];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Remote Notification Delegate // <= iOS 9.x

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSString *strDevicetoken = [[NSString alloc]initWithFormat:@"%@",[[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""]];
    NSLog(@"Device Token = %@",strDevicetoken);
    // Capture UserId And Assign Value to It.
    [Details setDeviceToken:strDevicetoken];
    [self registerForPush :strDevicetoken];
    self.strDeviceToken = strDevicetoken;
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"Push Notification Information : %@",userInfo);
    if(application.applicationState == UIApplicationStateActive) {
        
        //app is currently active, can update badges count here
        NSLog(@"didReceiveRemoteNotification called UIApplicationStateActive");
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"LoginToPush" bundle:nil];
        
        UIViewController *viewController = // determine the initial view controller here and instantiate it with
        [storyboard instantiateViewControllerWithIdentifier:@"LoginToPush"];
        
        self.window.rootViewController = viewController;
        [self.window makeKeyAndVisible];
        
    } else if(application.applicationState == UIApplicationStateBackground){
        NSLog(@"didReceiveRemoteNotification called UIApplicationStateBackground");
        //app is in background, if content-available key of your notification is set to 1, poll to your backend to retrieve data and update your interface here
        
        
    } else if(application.applicationState == UIApplicationStateInactive){
        NSLog(@"didReceiveRemoteNotification called UIApplicationStateInactive");
        //app is transitioning from background to foreground (user taps notification), do what you need when user taps here
        
        self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"LoginToPush" bundle:nil];
        
        UIViewController *viewController = // determine the initial view controller here and instantiate it with
        [storyboard instantiateViewControllerWithIdentifier:@"LoginToPush"];
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        self.window.rootViewController = viewController;
        [self.window makeKeyAndVisible];
        
    }
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"%@ = %@", NSStringFromSelector(_cmd), error);
    NSLog(@"Error = %@",error);
}

#pragma mark - UNUserNotificationCenter Delegate // >= iOS 10

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    NSLog(@"User Info = %@",notification.request.content.userInfo);
    NSDictionary *strPushInfo = notification.request.content.userInfo;
    
    NSLog(@"aps:%@ customeData:%@",[strPushInfo valueForKey:@"aps"],[strPushInfo valueForKey:@"customData"]);
    NSString *pushData = [strPushInfo objectForKey:@"customData"];
    NSString *apsData = [strPushInfo objectForKey:@"aps"];
    NSData *data = [pushData dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSLog(@"%@", [json objectForKey:@"userid"]);
    
    NSString *resultsUserID = [json objectForKey:@"userid"];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss a"];
    // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    NSString *currentDate = [dateFormatter stringFromDate:[NSDate date]];
    
    //
    
    
    NSMutableDictionary *pStore = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   resultsUserID,@"userid",
                                   pushData,@"pushData",
                                   apsData ,@"apsData",
                                   currentDate,@"currentDate", nil
                                   ];
    NSString *strJsonEncData = [self parseJSON:pStore];
    //added on 18/4/2014
    NSString *strPreviousJsonEncData = [self LoadFile:[@"currentPush" stringByAppendingString:resultsUserID]];
    NSString *newOutputString;
    if(strPreviousJsonEncData !=nil){
        newOutputString = [strPreviousJsonEncData stringByAppendingString:strJsonEncData];
    }else{
        newOutputString = strJsonEncData;
    }
    int iResult = [self WriteFile:[@"currentPush" stringByAppendingString:resultsUserID] usingWith:newOutputString];
     NSLog(@"File Write Result >> %d",iResult);
    completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    
    NSLog(@"User Info Response = %@",response.notification.request.content.userInfo);
    NSDictionary *strPushInfo = response.notification.request.content.userInfo;
    NSString *pushData = [strPushInfo objectForKey:@"customData"];
    NSString *apsData = [strPushInfo objectForKey:@"aps"];
    NSData *data = [pushData dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSLog(@"%@", [json objectForKey:@"userid"]);
    
    NSString *resultsUserID = [json objectForKey:@"userid"];
    NSString *resultsAction = [json objectForKey:@"Action"];
    NSString *status = 0;
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss a"];
    // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    NSString *currentDate = [dateFormatter stringFromDate:[NSDate date]];
    NSMutableDictionary *pStore = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   resultsUserID,@"userid",
                                   pushData,@"pushData",
                                   apsData ,@"apsData",
                                   currentDate,@"currentDate",
                                   status,@"status",
                                   nil
                                   ];
    // store dicitnory for action
    NSMutableDictionary *pAction = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   resultsUserID,@"userid",
                                   resultsAction,@"resultsAction",
                                    nil
                                   ];
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:pAction options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myString);
    
    NSMutableDictionary *Action = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   myString,resultsUserID, nil];
    Details.pushAction=Action;
    NSString *strJsonEncData = [self parseJSON:pStore];
    
    int iResult = [self WriteFile:[@"livePushDetails" stringByAppendingString:resultsUserID] usingWith:strJsonEncData] ;
    NSLog(@"File Write Result >> %d",iResult);
    
    NSUserDefaults *currentUL = [NSUserDefaults standardUserDefaults];
    [currentUL setObject:resultsUserID forKey:@"pushForTheUser"];
    // NSLog(@"userMPIN %@",[prefs stringForKey:_userId]);
    [currentUL synchronize];
    completionHandler();
//    dispatch_async(dispatch_get_main_queue(), ^{
//        LoginController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginToPush"];
//        [self presentViewController:vc animated:YES completion:nil];
//    });
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    LoginController *viewController = // determine the initial view controller here and instantiate it with
    [storyboard instantiateViewControllerWithIdentifier:@"LoginToPush"];
    
    self.window.rootViewController = viewController;
    [self.window makeKeyAndVisible];
    viewController.loginUserId.text = resultsUserID;
}

#pragma mark - Class Methods

/**
 Notification Registration
 */
- (void)registerForRemoteNotification {
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error ){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
            if(!granted){
                [self registerForPush:nil];
            }
        }];
    }
    else {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

-(void) registerForPush:(NSString *)strDevicetoken{
    
}

-(int) WriteFile:(NSString* )filename usingWith:(NSString* )filedata{
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/%@",
                          documentsDirectory,filename];
    //create content - four lines of text
    // NSString *content = @"One\nTwo\nThree\nFour\nFive";
    
    NSString *content = filedata;
    //save content to the documents directory
    bool result = [content writeToFile:fileName
                            atomically:NO
                              encoding:NSStringEncodingConversionAllowLossy
                                 error:nil];
    if(result == true){
        return 0;
    }else{
        return -1;
    }
    
}

-(NSString *)parseJSON:(NSMutableDictionary* )dictonary{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictonary options:NSJSONWritingPrettyPrinted error:&error];
    NSString *resultAsString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return resultAsString;
}

-(NSString *) LoadFile:(NSString *)filename{
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *strFileName = [NSString stringWithFormat:@"%@/%@",
                             documentsDirectory,filename];
    NSString *content = [[NSString alloc] initWithContentsOfFile:strFileName
                                                    usedEncoding:nil
                                                           error:nil];
    return content;
    //use simple alert from my library (see previous post for details)
    // [ASFunctions alert:content];
    //  [content release];
    
}


@end

