//
//  ThirdTourScreenViewController.m
//  AcledaPush
//
//  Created by Abhishek Ingle on 02/04/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import "ThirdTourScreenViewController.h"
#import "RegistrationController.h"

@interface ThirdTourScreenViewController ()

@end

@implementation ThirdTourScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)thirdTourFinish:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        RegistrationController  *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"registrationController"];
        [self presentViewController:vc animated:YES completion:nil];
    });
}
@end
