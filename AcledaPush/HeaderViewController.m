//
//  HeaderViewController.m
//  AcledaPush
//
//  Created by Abhishek Ingle on 24/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import "HeaderViewController.h"

@interface HeaderViewController ()

@end

@implementation HeaderViewController
@synthesize sidePanel,menuBtn,transV;
- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tapper = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideSidePanel:)];
    tapper.numberOfTapsRequired=1;
    [transV addGestureRecognizer:tapper];
    // Do any additional setup after loading the view.
}

- (void)hideSidePanel:(UIGestureRecognizer *)gesture{
    if(gesture.state == UIGestureRecognizerStateEnded){
        [transV setHidden:YES];
        [UIView transitionWithView:sidePanel duration:0.2 options:UIViewAnimationOptionCurveEaseIn animations:^{
            CGRect frame = sidePanel.frame;
            frame.origin.x = -sidePanel.frame.size.width;
            sidePanel.frame = frame;
        } completion:nil];
    }
}
-(IBAction)buttonPressed:(id)sender{
    if(sender == menuBtn){
        [transV setHidden:NO];
        [UIView transitionWithView:sidePanel duration:0.2 options:UIViewAnimationOptionCurveEaseIn animations:^{
            CGRect frame = sidePanel.frame;
            frame.origin.x = 0;
            sidePanel.frame = frame;
        } completion:nil];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
