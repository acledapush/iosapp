//
//  SuccessViewController.h
//  AcledaPush
//
//  Created by Abhishek Ingle on 24/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuccessViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *successMessage;
- (IBAction)homeRedirect:(id)sender;

@property ( nonatomic, strong ) NSString *userId;
@end
