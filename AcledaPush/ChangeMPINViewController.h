//
//  ChangeMPINViewController.h
//  AcledaPush
//
//  Created by Abhishek Ingle on 27/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangeMPINViewController : UIViewController <UITextFieldDelegate>{
    
}
@property (weak, nonatomic) IBOutlet UITextField *txtPass1;
@property (weak, nonatomic) IBOutlet UITextField *txtPass2;
@property (weak, nonatomic) IBOutlet UITextField *txtPass3;
@property (weak, nonatomic) IBOutlet UITextField *txtPass4;


@end
