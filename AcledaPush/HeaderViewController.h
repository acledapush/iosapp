//
//  HeaderViewController.h
//  AcledaPush
//
//  Created by Abhishek Ingle on 24/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderViewController : UIViewController{
    IBOutlet UIView *transV;
    IBOutlet UIView *sidePanel;
    IBOutlet UIButton *menuBtn;
}
@property(nonatomic)IBOutlet UIButton *menuBtn;
@property(nonatomic)IBOutlet UIView *sidePanel;
@property(nonatomic)IBOutlet UIView *transV;
@end
