//
//  TransactionViewCell.m
//  AcledaPush
//
//  Created by Abhishek Ingle on 23/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import "TransactionViewCell.h"

@implementation TransactionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void) updateCellWithTitle:(NSString *)title description:(NSString *)desc{
    self.descLbl.text = desc;
    self.titleLbl.text = title;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
