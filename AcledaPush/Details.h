//
//  Details.h
//  AcledaPush
//
//  Created by Abhishek Ingle on 13/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sys/utsname.h>

@interface Details : NSObject
+(void) setServerIp:(NSString*)value;
+(NSString*) serverIp;
+(void) setServerPort:(NSString*)value;
+(NSString*) serverPort;
+(void) setSecured:(NSString*)value;
+(NSString*) secured;
-(NSString*) getURL;
+(void) setDeviceToken:(NSString*)value;
+(NSString*) deviceToken;
-(NSString *)deviceModel;

+(void) setPushAction:(NSMutableDictionary*)value;
+(NSMutableDictionary*) pushAction;

@end
