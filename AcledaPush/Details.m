//
//  Details.m
//  AcledaPush
//
//  Created by Abhishek Ingle on 13/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import "Details.h"

@implementation Details

    static NSString* serverIp;
    static NSString* serverPort;
    static NSString* secured;
    static NSString* deviceToken;
    static NSMutableDictionary* pushAction;
    
+(void) setServerIp:(NSString*)value{
    serverIp=value;
}
+(void) setServerPort:(NSString*)value{
    serverPort=value;
}
+(void) setSecured:(NSString*)value{
    secured = value;
}

+(void) setDeviceToken:(NSString*)value{
    deviceToken = value;
}

+(void) setPushAction:(NSMutableDictionary*)value{
    pushAction = value;
}

+(NSString*) serverIp{
    return serverIp;
}
+(NSString*) serverPort{
    return serverPort;
}
+(NSString*) secured{
    return secured;
}

+(NSString*) deviceToken{
    return deviceToken;
}

+(NSMutableDictionary*) pushAction{
    return pushAction;
}



-(NSString*) getURL{    
    Details.secured = @"no";
    //ACLEDA UAT Web Server Details
   // Details.serverPort=@"8080";
    //Details.serverIp=@"110.74.212.29";
    
    //Demo Portal Server Details
    Details.serverPort=@"8080";
    Details.serverIp=@"128.199.222.136";
    
    //Details.serverIp=@"192.168.43.193";
    NSLog(@"%@",Details.serverIp);
    NSString* url=[NSString stringWithFormat:@"%@:%@/%@",Details.serverIp,Details.serverPort,@"PushConnector"];
    if([Details.secured isEqualToString:@"yes"]){
        url=[@"https://" stringByAppendingString:url];
    }else{
        url=[@"http://" stringByAppendingString:url];
    }
    NSLog(@"URL: %@" ,url);
    return url;
}
- (NSString *) deviceModel{
    struct utsname systemInfo;
    uname(&systemInfo);
    return [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
}

- (NSString *) platformString{
    NSString *platform = [self deviceModel];
     if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
     if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
     if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
     if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
     if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
     if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
     if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
     if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
     if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
     if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
     if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
     if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
     if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
     if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
     if ([platform isEqualToString:@"i386"])         return @"Simulator";
     if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    return platform;
}



@end
