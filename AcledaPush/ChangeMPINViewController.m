//
//  ChangeMPINViewController.m
//  AcledaPush
//
//  Created by Abhishek Ingle on 27/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import "ChangeMPINViewController.h"

@interface ChangeMPINViewController ()

@end

@implementation ChangeMPINViewController

-(void) nextFocus1 {
    [_txtPass2 becomeFirstResponder];
}

-(void) nextFocus2 {
    [_txtPass3 becomeFirstResponder];
}

-(void) nextFocus3 {
    [_txtPass4 becomeFirstResponder];
}

-(void) nextFocus4 {
    //check password
//    NSString* pass  = _txtPass4.text;
    [_txtPass4 resignFirstResponder];
//    EncryptAndDecrypt *tools = [[EncryptAndDecrypt alloc] init];
//    pass = [tools EncryptData:pass usingWith:PASS_ENCRYPT];
    //if ([pass isEqualToString:[self.setting getPassword]])  {
       // [self.setting saveCountLogin:0];
        
//    } else {
//        [self reset];
       // int countPass = [self.setting getCountLogin];
       // countPass++;
        //[self.setting saveCountLogin:countPass];
        //if (countPass >= 5) {
//            DestroyedViewController *controller = [[DestroyedViewController alloc] init];
//            controller.delegate = self.delegate;
//            [self.navigationController pushViewController:controller animated:NO];
//        } else {
//            if (countPass < 4) {
//                //[self showErrorMessage:@"Incorrect password, please enter again ?"];
//            } else {
//                //[self showErrorMessage:@"Last attempt now. Another wrong attempt with lock the token."];
//            }
//        }
    //}
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    _txtPass1.enabled = YES;
//    _txtPass2.enabled = NO;
//    _txtPass3.enabled = NO;
//    _txtPass4.enabled = NO;
   // txtPasscode.hidden = TRUE;
//    _txtPass1.text = @" ";
    _txtPass1.secureTextEntry = NO;
   //_txtPass2.text = @" ";
    _txtPass2.secureTextEntry = NO;
    //_txtPass3.text = @" ";
    _txtPass3.secureTextEntry = NO;
   // _txtPass4.text = @" ";
    _txtPass4.secureTextEntry = NO;
   // _txtPass1.delegate = self;
    [_txtPass1 becomeFirstResponder];
    
    [_txtPass1 addTarget:self action:@selector(passwordValueChanged) forControlEvents:UIControlEventEditingChanged];
    [_txtPass2 addTarget:self action:@selector(passwordValueChanged2) forControlEvents:UIControlEventEditingChanged];
    [_txtPass3 addTarget:self action:@selector(passwordValueChanged3) forControlEvents:UIControlEventEditingChanged];
    [_txtPass4 addTarget:self action:@selector(passwordValueChanged4) forControlEvents:UIControlEventEditingChanged];
}
-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //    [txtPasscode resignFirstResponder];
    
}
- (void)passwordValueChanged2{
    NSString* pinTwo = _txtPass2.text;
    if(![pinTwo isEqualToString:@""]){
        _txtPass2.secureTextEntry = YES;
//        _txtPass3.secureTextEntry = NO;
//        _txtPass4.secureTextEntry = NO;
        [self nextFocus2];
    }
}
- (void)passwordValueChanged3{
    NSString* pinThree = _txtPass3.text;
    if(![pinThree isEqualToString:@""]){
        _txtPass3.secureTextEntry = YES;
        //_txtPass4.secureTextEntry = NO;
        [self nextFocus3];
    }
}
- (void)passwordValueChanged4{
    NSString* pinFour = _txtPass4.text;
    if(![pinFour isEqualToString:@""]){
        _txtPass4.secureTextEntry = YES;
        [self nextFocus4];
    }
}
- (void)passwordValueChanged{
    //NSUInteger newLength = txtPasscode.text.length; //oldLength - rangeLength + replacementLength;
        NSString* pinOne = _txtPass1.text;
    if(![pinOne isEqualToString:@""]){
        _txtPass1.secureTextEntry = YES;
//        _txtPass2.secureTextEntry = NO;
//        _txtPass3.secureTextEntry = NO;
//        _txtPass4.secureTextEntry = NO;
        [self nextFocus1];
    }
}

-(void) reset {
    _txtPass1.secureTextEntry = NO;
    _txtPass2.secureTextEntry = NO;
    _txtPass3.secureTextEntry = NO;
    _txtPass4.secureTextEntry = NO;
}
- (BOOL)textFieldShouldClear:(UITextField *)textField {
    return YES;
}
//- (BOOL)connected
//{
//    Reachability *reachability = [Reachability reachabilityForInternetConnection];
//    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
//    return networkStatus != NotReachable;
//}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSLog(@"Test app");
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    NSLog(@"Test app 1");
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag<10) {
        if ((textField.text.length >= 1) && (string.length > 0))
        {
            NSInteger nextText = textField.tag + 1;
            // Try to find next responder
            UIResponder* nextResponder = [textField.superview viewWithTag:nextText];
            if (! nextResponder)
                [textField resignFirstResponder];
            // nextResponder = [textField.superview viewWithTag:1];
            if (nextResponder){
                // Found next responder, so set it.
                [nextResponder becomeFirstResponder];
                UITextField *nextTextfield= (UITextField*) [textField.superview viewWithTag:nextText];
                if (nextTextfield.text.length<1) {
                    if(nextTextfield.tag==4){
                        [nextTextfield setText:string];
                        [nextTextfield resignFirstResponder];
                    }else{
                        [nextTextfield setText:string];
                    }
                }
                return NO;
            }
        }
    }
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
