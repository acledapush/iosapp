//
//  RegisterSuccessScreen.m
//  AcledaPush
//
//  Created by Abhishek Ingle on 28/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import "RegisterSuccessScreen.h"
#import "LoginController.h"
#import "RegistrationController.h"

@interface RegisterSuccessScreen ()

@end

@implementation RegisterSuccessScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)goTOLoginScreen:(id)sender {
    LoginController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginToPush"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)registerAnotherUser:(id)sender {
    RegistrationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"registrationController"];
    [self presentViewController:vc animated:YES completion:nil];
}
@end
