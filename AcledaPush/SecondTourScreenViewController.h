//
//  SecondTourScreenViewController.h
//  AcledaPush
//
//  Created by Abhishek Ingle on 02/04/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondTourScreenViewController : UIViewController
- (IBAction)sceondTourNext:(id)sender;
- (IBAction)secondTourSkip:(id)sender;


@end
