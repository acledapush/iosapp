//
//  PushHistoryViewController.h
//  AcledaPush
//
//  Created by Abhishek Ingle on 29/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PushHistoryViewController : UIViewController

@property (weak,nonatomic) IBOutlet UIBarButtonItem *barButton;
@property (weak, nonatomic) IBOutlet UILabel *pushHistoryUserId;

@end
