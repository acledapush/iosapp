//
//  FirstTourScreenViewController.m
//  AcledaPush
//
//  Created by Abhishek Ingle on 02/04/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import "FirstTourScreenViewController.h"
#import "RegistrationController.h"
#import "SecondTourScreenViewController.h"
@interface FirstTourScreenViewController ()

@end

@implementation FirstTourScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)firstTourNext:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        SecondTourScreenViewController  *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"secondTour"];
        [self presentViewController:vc animated:YES completion:nil];
    });
}

- (IBAction)firstTourSkip:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        RegistrationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"registrationController"];
        [self presentViewController:vc animated:YES completion:nil];
    });
}
@end
