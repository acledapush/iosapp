//
//  RegistrationController.m
//  AcledaPush
//
//  Created by Abhishek Ingle on 14/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import "RegistrationController.h"
#import "Details.h"
#import "VerifyRegCodeController.h"

@interface RegistrationController ()
{
    NSArray *arr;
    NSDictionary *codes;
}
@end

@implementation RegistrationController

static NSString *keyboardOpened=nil;

+(NSString*)keyboardOpened{return  keyboardOpened;}
+(void)setKeyboardOpened:(NSString*)value{
    keyboardOpened=value;
}

int tokenCategory=4;
int tokenSubcategory=1;
int regCodeOnEmail=4;
int regCodeOnSMS=1;
int regCodeOnSMSAndEmail=2;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arr = @[ @"Cambodia",@"Abkhazia",@"Afghanistan",@"Albania",@"Algeria",@"American Samoa",@"Andorra",@"Angola",@"Anguilla",@"Antigua and Barbuda",@"Argentina",
             @"Armenia",@"Aruba",@"Ascension",@"Australia",@"Australian External Territories",@"Austria",@"Azerbaijan",                    @"Bahamas",@"Bahrain",@"Bangladesh",@"Barbados",@"Barbuda",@"Belarus",@"Belgium",@"Belize",@"Benin",@"Bermuda",@"Bhutan",@"Bolivia",@"Bosnia and Herzegovina",@"Botswana",@"Brazil",@"British Indian Ocean Territory",@"British Virgin Islands",@"British Virgin Islands",@"Brunei",@"Bulgaria",@"Burkina Faso",@"Burundi",
             @"Cameroon",@"Canada",@"Cape Verde",@"Cayman Islands",@"Central African Republic",@"Chad",@"Chile",@"China",@"Christmas Island",@"Cocos-Keeling Islands",@"Colombia",@"Comoros",
             @"Congo",@"Congo, Dem. Rep. of (Zaire)",@"Cook Islands",@"Costa Rica",
             @"Croatia",@"Cuba",@"Curacao",@"Cyprus",@"Czech Republic",
             @"Denmark",@"Diego Garcia",@"Djibouti",@"Dominica",@"Dominican Republic",
             @"East Timor",@"Easter Island",@"Ecuador",@"Egypt",@"El Salvador",@"Equatorial Guinea",@"Eritrea",@"Estonia",@"Ethiopia",
             @"Gabon", @"Gambia",@"Georgia", @"Germany",@"Ghana",@"Gibraltar",@"Greece",
             @"Greenland",@"Grenada",@"Guadeloupe",@"Guam",@"Guatemala",@"Guinea",
             @"Guinea-Bissau",@"Guyana",
             @"Haiti",@"Honduras",@"Hong Kong SAR China",@"Hungary",
             @"Falkland Islands",@"Faroe Islands",@"Fiji",@"Finland",@"France",@"French Antilles",@"French Guiana",@"French Polynesia",
             @"Iceland",@"India",@"Indonesia",@"Iran",@"Iraq",@"Ireland",@"Israel",@"Italy",@"Ivory Coast",
             @"Jamaica",@"Japan",@"Jordan",@"Kazakhstan",@"Kenya",@"Kiribati",@"North Korea",@"South Korea",@"Kuwait",@"Kyrgyzstan",            @"Laos",@"Latvia",@"Lebanon",@"Lesotho",@"Liberia",@"Libya",@"Liechtenstein",@"Lithuania",@"Luxembourg",@"Macau SAR China",@"Macedonia",@"Madagascar",@"Malawi",@"Malaysia",@"Maldives",@"Mali",@"Malta",@"Marshall Islands",@"Martinique",
             @"Mauritania",@"Mauritius",@"Mayotte",@"Mexico",@"Micronesia",@"Midway Island",@"Micronesia",@"Moldova",@"Monaco",@"Mongolia",@"Montenegro",@"Montserrat",@"Morocco",@"Myanmar",@"Namibia",@"Nauru",@"Nepal",@"Netherlands",@"Netherlands Antilles",@"Nevis",@"New Caledonia",@"New Zealand",@"Nicaragua",@"Niger",@"Nigeria",@"Niue",@"Norfolk Island",@"Northern Mariana Islands",@"Norway",
             @"Oman",@"Palau",@"Palestinian Territory",@"Panama",@"Papua New Guinea",@"Paraguay",@"Peru",@"Philippines",@"Poland",@"Portugal",@"Puerto Rico",
             @"Qatar",@"Reunion",@"Romania",@"Russia",@"Rwanda",@"Samoa",@"San Marino",@"Saudi Arabia",@"Senegal",@"Serbia",@"Seychelles",@"Sierra Leone",@"Singapore",@"Slovakia",@"Slovenia",@"Solomon Islands",@"South Africa",@"South Georgia and the South Sandwich Islands",@"Spain",@"Sri Lanka",@"Sudan",@"Suriname",@"Swaziland",@"Sweden",@"Switzerland",@"Syria",
             @"Taiwan",@"Tajikistan",@"Tanzania",@"Thailand",@"Timor Leste",@"Togo",@"Tokelau",@"Tonga",@"Trinidad and Tobago",@"Tunisia",@"Turkey",@"Turkmenistan",@"Turks and Caicos Islands",@"Tuvalu",@"Uganda",@"Ukraine",@"United Arab Emirates",@"United Kingdom",@"United States",@"Uruguay",@"U.S. Virgin Islands",@"Uzbekistan",@"Vanuatu",@"Venezuela",@"Vietnam",@"Wake Island",@"Wallis and Futuna",@"Yemen",@"Zambia",@"Zanzibar",@"Zimbabwe"
             ];
    codes = @{
              @"Canada"                                       : @"+1",
              @"China"                                        : @"+86",
              @"France"                                       : @"+33",
              @"Germany"                                      : @"+49",
              @"India"                                        : @"+91",
              @"Japan"                                        : @"+81",
              @"Pakistan"                                     : @"+92",
              @"United Kingdom"                               : @"+44",
              @"United States"                                : @"+1",
              @"Abkhazia"                                     : @"+7 840",
              @"Abkhazia"                                     : @"+7 940",
              @"Afghanistan"                                  : @"+93",
              @"Albania"                                      : @"+355",
              @"Algeria"                                      : @"+213",
              @"American Samoa"                               : @"+1 684",
              @"Andorra"                                      : @"+376",
              @"Angola"                                       : @"+244",
              @"Anguilla"                                     : @"+1 264",
              @"Antigua and Barbuda"                          : @"+1 268",
              @"Argentina"                                    : @"+54",
              @"Armenia"                                      : @"+374",
              @"Aruba"                                        : @"+297",
              @"Ascension"                                    : @"+247",
              @"Australia"                                    : @"+61",
              @"Australian External Territories"              : @"+672",
              @"Austria"                                      : @"+43",
              @"Azerbaijan"                                   : @"+994",
              @"Bahamas"                                      : @"+1 242",
              @"Bahrain"                                      : @"+973",
              @"Bangladesh"                                   : @"+880",
              @"Barbados"                                     : @"+1 246",
              @"Barbuda"                                      : @"+1 268",
              @"Belarus"                                      : @"+375",
              @"Belgium"                                      : @"+32",
              @"Belize"                                       : @"+501",
              @"Benin"                                        : @"+229",
              @"Bermuda"                                      : @"+1 441",
              @"Bhutan"                                       : @"+975",
              @"Bolivia"                                      : @"+591",
              @"Bosnia and Herzegovina"                       : @"+387",
              @"Botswana"                                     : @"+267",
              @"Brazil"                                       : @"+55",
              @"British Indian Ocean Territory"               : @"+246",
              @"British Virgin Islands"                       : @"+1 284",
              @"Brunei"                                       : @"+673",
              @"Bulgaria"                                     : @"+359",
              @"Burkina Faso"                                 : @"+226",
              @"Burundi"                                      : @"+257",
              @"Cambodia"                                     : @"+855",
              @"Cameroon"                                     : @"+237",
              @"Canada"                                       : @"+1",
              @"Cape Verde"                                   : @"+238",
              @"Cayman Islands"                               : @"+ 345",
              @"Central African Republic"                     : @"+236",
              @"Chad"                                         : @"+235",
              @"Chile"                                        : @"+56",
              @"China"                                        : @"+86",
              @"Christmas Island"                             : @"+61",
              @"Cocos-Keeling Islands"                        : @"+61",
              @"Colombia"                                     : @"+57",
              @"Comoros"                                      : @"+269",
              @"Congo"                                        : @"+242",
              @"Congo, Dem. Rep. of (Zaire)"                  : @"+243",
              @"Cook Islands"                                 : @"+682",
              @"Costa Rica"                                   : @"+506",
              @"Ivory Coast"                                  : @"+225",
              @"Croatia"                                      : @"+385",
              @"Cuba"                                         : @"+53",
              @"Curacao"                                      : @"+599",
              @"Cyprus"                                       : @"+537",
              @"Czech Republic"                               : @"+420",
              @"Denmark"                                      : @"+45",
              @"Diego Garcia"                                 : @"+246",
              @"Djibouti"                                     : @"+253",
              @"Dominica"                                     : @"+1 767",
              @"Dominican Republic"                           : @"+1 809",
              @"Dominican Republic"                           : @"+1 829",
              @"Dominican Republic"                           : @"+1 849",
              @"East Timor"                                   : @"+670",
              @"Easter Island"                                : @"+56",
              @"Ecuador"                                      : @"+593",
              @"Egypt"                                        : @"+20",
              @"El Salvador"                                  : @"+503",
              @"Equatorial Guinea"                            : @"+240",
              @"Eritrea"                                      : @"+291",
              @"Estonia"                                      : @"+372",
              @"Ethiopia"                                     : @"+251",
              @"Falkland Islands"                             : @"+500",
              @"Faroe Islands"                                : @"+298",
              @"Fiji"                                         : @"+679",
              @"Finland"                                      : @"+358",
              @"France"                                       : @"+33",
              @"French Antilles"                              : @"+596",
              @"French Guiana"                                : @"+594",
              @"French Polynesia"                             : @"+689",
              @"Gabon"                                        : @"+241",
              @"Gambia"                                       : @"+220",
              @"Georgia"                                      : @"+995",
              @"Germany"                                      : @"+49",
              @"Ghana"                                        : @"+233",
              @"Gibraltar"                                    : @"+350",
              @"Greece"                                       : @"+30",
              @"Greenland"                                    : @"+299",
              @"Grenada"                                      : @"+1 473",
              @"Guadeloupe"                                   : @"+590",
              @"Guam"                                         : @"+1 671",
              @"Guatemala"                                    : @"+502",
              @"Guinea"                                       : @"+224",
              @"Guinea-Bissau"                                : @"+245",
              @"Guyana"                                       : @"+595",
              @"Haiti"                                        : @"+509",
              @"Honduras"                                     : @"+504",
              @"Hong Kong SAR China"                          : @"+852",
              @"Hungary"                                      : @"+36",
              @"Iceland"                                      : @"+354",
              @"India"                                        : @"+91",
              @"Indonesia"                                    : @"+62",
              @"Iran"                                         : @"+98",
              @"Iraq"                                         : @"+964",
              @"Ireland"                                      : @"+353",
              @"Israel"                                       : @"+972",
              @"Italy"                                        : @"+39",
              @"Jamaica"                                      : @"+1 876",
              @"Japan"                                        : @"+81",
              @"Jordan"                                       : @"+962",
              @"Kazakhstan"                                   : @"+7 7",
              @"Kenya"                                        : @"+254",
              @"Kiribati"                                     : @"+686",
              @"North Korea"                                  : @"+850",
              @"South Korea"                                  : @"+82",
              @"Kuwait"                                       : @"+965",
              @"Kyrgyzstan"                                   : @"+996",
              @"Laos"                                         : @"+856",
              @"Latvia"                                       : @"+371",
              @"Lebanon"                                      : @"+961",
              @"Lesotho"                                      : @"+266",
              @"Liberia"                                      : @"+231",
              @"Libya"                                        : @"+218",
              @"Liechtenstein"                                : @"+423",
              @"Lithuania"                                    : @"+370",
              @"Luxembourg"                                   : @"+352",
              @"Macau SAR China"                              : @"+853",
              @"Macedonia"                                    : @"+389",
              @"Madagascar"                                   : @"+261",
              @"Malawi"                                       : @"+265",
              @"Malaysia"                                     : @"+60",
              @"Maldives"                                     : @"+960",
              @"Mali"                                         : @"+223",
              @"Malta"                                        : @"+356",
              @"Marshall Islands"                             : @"+692",
              @"Martinique"                                   : @"+596",
              @"Mauritania"                                   : @"+222",
              @"Mauritius"                                    : @"+230",
              @"Mayotte"                                      : @"+262",
              @"Mexico"                                       : @"+52",
              @"Micronesia"                                   : @"+691",
              @"Midway Island"                                : @"+1 808",
              @"Micronesia"                                   : @"+691",
              @"Moldova"                                      : @"+373",
              @"Monaco"                                       : @"+377",
              @"Mongolia"                                     : @"+976",
              @"Montenegro"                                   : @"+382",
              @"Montserrat"                                   : @"+1664",
              @"Morocco"                                      : @"+212",
              @"Myanmar"                                      : @"+95",
              @"Namibia"                                      : @"+264",
              @"Nauru"                                        : @"+674",
              @"Nepal"                                        : @"+977",
              @"Netherlands"                                  : @"+31",
              @"Netherlands Antilles"                         : @"+599",
              @"Nevis"                                        : @"+1 869",
              @"New Caledonia"                                : @"+687",
              @"New Zealand"                                  : @"+64",
              @"Nicaragua"                                    : @"+505",
              @"Niger"                                        : @"+227",
              @"Nigeria"                                      : @"+234",
              @"Niue"                                         : @"+683",
              @"Norfolk Island"                               : @"+672",
              @"Northern Mariana Islands"                     : @"+1 670",
              @"Norway"                                       : @"+47",
              @"Oman"                                         : @"+968",
              @"Pakistan"                                     : @"+92",
              @"Palau"                                        : @"+680",
              @"Palestinian Territory"                        : @"+970",
              @"Panama"                                       : @"+507",
              @"Papua New Guinea"                             : @"+675",
              @"Paraguay"                                     : @"+595",
              @"Peru"                                         : @"+51",
              @"Philippines"                                  : @"+63",
              @"Poland"                                       : @"+48",
              @"Portugal"                                     : @"+351",
              @"Puerto Rico"                                  : @"+1 787",
              @"Puerto Rico"                                  : @"+1 939",
              @"Qatar"                                        : @"+974",
              @"Reunion"                                      : @"+262",
              @"Romania"                                      : @"+40",
              @"Russia"                                       : @"+7",
              @"Rwanda"                                       : @"+250",
              @"Samoa"                                        : @"+685",
              @"San Marino"                                   : @"+378",
              @"Saudi Arabia"                                 : @"+966",
              @"Senegal"                                      : @"+221",
              @"Serbia"                                       : @"+381",
              @"Seychelles"                                   : @"+248",
              @"Sierra Leone"                                 : @"+232",
              @"Singapore"                                    : @"+65",
              @"Slovakia"                                     : @"+421",
              @"Slovenia"                                     : @"+386",
              @"Solomon Islands"                              : @"+677",
              @"South Africa"                                 : @"+27",
              @"South Georgia and the South Sandwich Islands" : @"+500",
              @"Spain"                                        : @"+34",
              @"Sri Lanka"                                    : @"+94",
              @"Sudan"                                        : @"+249",
              @"Suriname"                                     : @"+597",
              @"Swaziland"                                    : @"+268",
              @"Sweden"                                       : @"+46",
              @"Switzerland"                                  : @"+41",
              @"Syria"                                        : @"+963",
              @"Taiwan"                                       : @"+886",
              @"Tajikistan"                                   : @"+992",
              @"Tanzania"                                     : @"+255",
              @"Thailand"                                     : @"+66",
              @"Timor Leste"                                  : @"+670",
              @"Togo"                                         : @"+228",
              @"Tokelau"                                      : @"+690",
              @"Tonga"                                        : @"+676",
              @"Trinidad and Tobago"                          : @"+1 868",
              @"Tunisia"                                      : @"+216",
              @"Turkey"                                       : @"+90",
              @"Turkmenistan"                                 : @"+993",
              @"Turks and Caicos Islands"                     : @"+1 649",
              @"Tuvalu"                                       : @"+688",
              @"Uganda"                                       : @"+256",
              @"Ukraine"                                      : @"+380",
              @"United Arab Emirates"                         : @"+971",
              @"United Kingdom"                               : @"+44",
              @"United States"                                : @"+1",
              @"Uruguay"                                      : @"+598",
              @"U.S. Virgin Islands"                          : @"+1 340",
              @"Uzbekistan"                                   : @"+998",
              @"Vanuatu"                                      : @"+678",
              @"Venezuela"                                    : @"+58",
              @"Vietnam"                                      : @"+84",
              @"Wake Island"                                  : @"+1 808",
              @"Wallis and Futuna"                            : @"+681",
              @"Yemen"                                        : @"+967",
              @"Zambia"                                       : @"+260",
              @"Zanzibar"                                     : @"+255",
              @"Zimbabwe"                                     : @"+263"
              };
    
    self.countryCode.text = codes[@"Cambodia"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return arr.count;
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return arr[row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSString *countryName = arr[row];
    self.countryCode.text = codes[countryName];
}

-(NSString*) getUser{
    NSLog(@"User ID %@",self.userid.text);
    NSString *uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *strdata =[NSString stringWithFormat:@"{\"_devicetoken\":\"%@\",\"_deviceId\":\"%@\"}",Details.deviceToken,uniqueIdentifier];
    NSLog(@"JSONString %@",strdata);
    NSCharacterSet * queryKVSet = [NSCharacterSet
                                   characterSetWithCharactersInString:@"+"
                                   ].invertedSet;
    
    NSString * parameterString = [NSString
                                  stringWithFormat:@"userid=%@&devicePayload=%@",
                                  _userid.text,strdata
                                  ];
    NSLog(@"%@", parameterString);
    NSString* encodedUrl = [parameterString stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet];
    
    NSData *postData = [encodedUrl dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
    
    NSString *postLength=[NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    Details* details=[[Details alloc]init];
    NSString* urlDetails = [details getURL];
    NSString* post_url= [urlDetails stringByAppendingString:@"/GetUserBy"];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@",post_url]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString* aStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] ;
    NSLog(@"responseData %@",aStr);
    return aStr;
}

-(NSString*) resendActivationCodeOnEmailAndSMS{
    
    NSCharacterSet * queryKVSet = [NSCharacterSet
                                   characterSetWithCharactersInString:@"+"
                                   ].invertedSet;
    
    NSString * parameterString = [NSString                                  stringWithFormat:@"userid=%@&category=%d&subcategory=%d&type=%d",
                                  _userid.text,tokenCategory,tokenSubcategory,regCodeOnSMSAndEmail
                                  ];
    NSLog(@"%@", parameterString);
    NSString* encodedUrl = [parameterString stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet];
    
    NSData *postData = [encodedUrl dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
    
    NSString *postLength=[NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    Details* details=[[Details alloc]init];
    NSString* urlDetails = [details getURL];
    NSString* post_url= [urlDetails stringByAppendingString:@"/ResendActivationCode"];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@",post_url]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString* aStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] ;
    NSLog(@"responseData %@",aStr);
    return aStr;
}

//-(NSString*) resendActivationCodeOnSMS{
//
//    NSCharacterSet * queryKVSet = [NSCharacterSet
//                                   characterSetWithCharactersInString:@"+"
//                                   ].invertedSet;
//
//    NSString * parameterString = [NSString                                  stringWithFormat:@"userid=%@&category=%d&subcategory=%d&type=%d",
//                                  _userid.text,tokenCategory,tokenSubcategory,regCodeOnSMS
//                                  ];
//    NSLog(@"%@", parameterString);
//    NSString* encodedUrl = [parameterString stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet];
//
//    NSData *postData = [encodedUrl dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
//
//    NSString *postLength=[NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
//    Details* details = [[Details alloc]init];
//    NSString* urlDetails = [details getURL];
//    NSString* post_url= [urlDetails stringByAppendingString:@"/ResendActivationCode"];
//    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@",post_url]];
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
//    [request setURL:url];
//    [request setHTTPMethod:@"POST"];
//    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
//    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
//    [request setHTTPBody:postData];
//
//    NSError *error;
//    NSURLResponse *response;
//    NSData *responseData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//    NSString* aStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] ;
//    NSLog(@"responseData %@",aStr);
//    return aStr;
//}

-(BOOL) textFieldShouldReturn: (UITextField *) textField
{
    [textField resignFirstResponder];
    if(keyboardOpened!=nil && [keyboardOpened isEqualToString:@"YES"]){
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
        keyboardOpened=nil;
    }
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField ==_mobileno || textField == _emailid){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        keyboardOpened=@"YES";
    }
//
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -keyboardSize.height;
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}
UIAlertController *waitAlertController;

-(NSString*) getUserPhoneNumber{
    
    NSString *userPhone = _mobileno.text;
    NSString *userCountryCode = _countryCode.text;
    
    if([userCountryCode isEqualToString:@"+855"]){
        if([userPhone hasPrefix:@"0"]){
            userPhone = [userPhone substringFromIndex:1];
        }
    }
    NSString *resultantString = [NSString stringWithFormat:@"%@%@",userCountryCode,userPhone];
    NSLog(@"resultantString %@ ", resultantString);
    return resultantString;
                                 
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _userid){
        NSRange lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
        
        if (lowercaseCharRange.location != NSNotFound) {
            
            if (textField.text.length == 0) {
                
                // Add first character -- and update Return button
                //
                // But this has the effect of losing the editing point
                // (only when trying to edit with lowercase characters),
                // because the text of the UITextField is modified,
                // which does not matter since the caret is at the end
                // of the (empty) text fiueld anyways.
                //
                textField.text = [textField.text stringByReplacingCharactersInRange:range withString:[string uppercaseString]];
                
            } else {
                
                // Replace range instead of whole string in order
                // not to lose input caret position -- but this will
                // not update the Return button, which is not necessary
                // here anyways
                //
                UITextPosition *beginning = textField.beginningOfDocument;
                UITextPosition *start = [textField positionFromPosition:beginning offset:range.location];
                UITextPosition *end = [textField positionFromPosition:start offset:range.length];
                UITextRange *textRange = [textField textRangeFromPosition:start toPosition:end];
                
                [textField replaceRange:textRange withText:[string uppercaseString]];
            }
            
            return NO;
        }
        return YES;
    }
    return YES;
}
-(void) doRegistration{
    NSString *updatedPhoneNumber = self.getUserPhoneNumber;
    NSString *response = self.getUser;
    NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSString* strResult=[json objectForKey:@"result"];
    NSLog(@"strResult %@",strResult);
    if([strResult isEqualToString:@"success"]){
        NSString* strphoneNumber=[json objectForKey:@"phoneNumber"];
        NSString* strEmailId=[json objectForKey:@"emailId"];
        
        // check enter email id and phone no are vaild
        if(![strphoneNumber isEqualToString:updatedPhoneNumber]||![strEmailId isEqualToString:_emailid.text]){
            [self dismissViewControllerAnimated:waitAlertController completion:^{
                UIAlertController *failureAlertController =nil;
                failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Your emailId or mobile number does not match with this User Id" preferredStyle:UIAlertControllerStyleAlert];
                [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:failureAlertController animated:YES completion:nil];
            }];
        }else{
            NSString *responseForRegCodeSending = self.resendActivationCodeOnEmailAndSMS;
            
            NSData *dataForEmail = [responseForRegCodeSending dataUsingEncoding:NSUTF8StringEncoding];
            id jsonForEmail = [NSJSONSerialization JSONObjectWithData:dataForEmail options:0 error:nil];
            NSString* strResultForEmail=[jsonForEmail objectForKey:@"result"];
            NSLog(@"strResultForEmail %@",strResultForEmail);
            if([strResultForEmail isEqualToString:@"success"]){                                
                NSUserDefaults *currentURegister = [NSUserDefaults standardUserDefaults];
                NSString* currentRegUser =[_userid.text stringByTrimmingCharactersInSet:
                                           [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                [currentURegister setObject:currentRegUser forKey:@"currentRegisterInUser"];
                [currentURegister synchronize];
                NSLog(@"currentRegisterUser >> %@",currentRegUser);
                [self dismissViewControllerAnimated:waitAlertController completion:^{
                    UIAlertController *failureAlertController =nil;
                    failureAlertController = [UIAlertController alertControllerWithTitle:@"Success" message:@"We sent the registration code via SMS and Email, Please Check" preferredStyle:UIAlertControllerStyleAlert];
                    [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            VerifyRegCodeController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"verifyRegCode"];
                            vc.userId=[_userid.text stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            [self presentViewController:vc animated:YES completion:nil];
                            vc.userIdLable.text = _userid.text;
                            vc.userPhoneLable.text = self.getUserPhoneNumber;
                        });
                    }]];
                    [self presentViewController:failureAlertController animated:YES completion:nil];
                }];
            }else if ([strResultForEmail isEqualToString:@"error"]){
                [self dismissViewControllerAnimated:waitAlertController completion:^{
                    UIAlertController *failureAlertController =nil;
                    failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:[json objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                    [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                    [self presentViewController:failureAlertController animated:YES completion:nil];
                }];
            }else{
                [self dismissViewControllerAnimated:waitAlertController completion:^{
                    UIAlertController *failureAlertController =nil;
                    failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Unable to send registration code" preferredStyle:UIAlertControllerStyleAlert];
                    [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                    [self presentViewController:failureAlertController animated:YES completion:nil];
                }];
            }
        }
    }else if(strResult == nil){
        [self dismissViewControllerAnimated:waitAlertController completion:^{
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Server is not responding" preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
        }];
    }else{
        [self dismissViewControllerAnimated:waitAlertController completion:^{
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:[json objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
        }];
    }
}
- (IBAction)submit:(id)sender {
    NSLog(@"Registration for Push");
    waitAlertController = [UIAlertController alertControllerWithTitle:@"Please wait" message:@"Registration is in process ..." preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:waitAlertController animated:YES completion:^{
    dispatch_async(dispatch_get_main_queue(), ^{[self doRegistration];});
    }];}
@end
