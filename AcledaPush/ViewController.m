//
//  ViewController.m
//  AcledaPush
//
//  Created by vikram sareen on 2/20/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import "ViewController.h"
#import "Details.h"

@interface ViewController ()

@end

@implementation ViewController

static NSString *keyboardOpened=nil;

+(NSString*)keyboardOpened{return  keyboardOpened;}
+(void)setKeyboardOpened:(NSString*)value{
    keyboardOpened=value;
}

int tokenCategoryV2=3;
int tokenSubcategoryV2=5;
int regCodeOnEmailV2=4;
int regCodeOnSMSV2=2;
- (void)viewDidLoad {
    [super viewDidLoad];
    Details.secured = @"yes";
    Details.serverPort=@"6443";
    Details.serverIp=@"localhost";
    NSLog(@"%d",tokenSubcategoryV2);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSString*) getURLV2{
    NSString* url=[NSString stringWithFormat:@"%@:%@/%@/%@",Details.serverIp,Details.serverPort,@"PushConnector"];
    if(Details.secured){
        url=[@"https://" stringByAppendingString:url];
    }else{
        url=[@"http://" stringByAppendingString:url];
    }
    //NSLog(@"URL: %@" ,url);
    return url;
}

- (IBAction)submit:(id)sender {
    NSLog(@"Registration for Push");
    NSString *response = self.getUserV2;
    NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSString* strResult=[json objectForKey:@"result"];
    if([strResult isEqualToString:@"success"]){
        NSString *responseForRegCodeOnEmail = self.resendActivationCodeOnEmailV2;
        NSString *responseForRegCodeOnSMS = self.resendActivationCodeOnSMSV2;
        
        NSData *dataForSMS = [responseForRegCodeOnSMS dataUsingEncoding:NSUTF8StringEncoding];
        id jsonForSMS = [NSJSONSerialization JSONObjectWithData:dataForSMS options:0 error:nil];
        NSString* strResultForSMS=[jsonForSMS objectForKey:@"result"];
        
        NSData *dataForEmail = [responseForRegCodeOnEmail dataUsingEncoding:NSUTF8StringEncoding];
        id jsonForEmail = [NSJSONSerialization JSONObjectWithData:dataForEmail options:0 error:nil];
        NSString* strResultForEmail=[jsonForEmail objectForKey:@"result"];
        
        if([strResultForSMS isEqualToString:@"success"]&&[strResultForEmail isEqualToString:@"success"]){
        
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"ALERT" message:@"We sended the registration code on SMS and Email, Please Check" preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                dispatch_async(dispatch_get_main_queue(), ^{
                    ViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"verifyRegCode"];
                
                    [self presentViewController:vc animated:YES completion:nil];
                });
            }]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
        }else if([strResultForEmail isEqualToString:@"success"]&&[strResultForSMS isEqualToString:@"error"]){
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"ALERT" message:@"We sended the registration code on Email, Please Check" preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                dispatch_async(dispatch_get_main_queue(), ^{
                    ViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"verifyRegCode"];
                    
                    [self presentViewController:vc animated:YES completion:nil];
                });
            }]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
        }else if ([strResultForEmail isEqualToString:@"error"]&&[strResultForSMS isEqualToString:@"success"]){
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"ALERT" message:@"We sended the registration code on SMS, Please Check" preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                dispatch_async(dispatch_get_main_queue(), ^{
                    ViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"verifyRegCode"];
                    
                    [self presentViewController:vc animated:YES completion:nil];
                });
            }]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
        }
        
    }else{
        UIAlertController *failureAlertController =nil;
        failureAlertController = [UIAlertController alertControllerWithTitle:@"ALERT" message:@"You are not a valid user" preferredStyle:UIAlertControllerStyleAlert];
        [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:failureAlertController animated:YES completion:nil];
    }
}

-(NSString*) getUserV2{
    NSLog(@"User ID %@",self.userid.text);
    
    NSCharacterSet * queryKVSet = [NSCharacterSet
                                   characterSetWithCharactersInString:@"+"
                                   ].invertedSet;

    NSString * parameterString = [NSString
            stringWithFormat:@"userid=%@",
                             _userid.text
    ];
    NSLog(@"%@", parameterString);
    NSString* encodedUrl = [parameterString stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet];
    
    NSData *postData = [encodedUrl dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
    
    NSString *postLength=[NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSString* urlDetails = self.getURLV2;
    NSString* post_url= [NSString stringWithFormat:urlDetails,@"/GetUserBy"];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@",post_url]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString* aStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] ;
    NSLog(@"responseData %@",aStr);
    return aStr;
}

-(NSString*) resendActivationCodeOnEmailV2{
    
    NSCharacterSet * queryKVSet = [NSCharacterSet
                                   characterSetWithCharactersInString:@"+"
                                   ].invertedSet;
    
    NSString * parameterString = [NSString                                  stringWithFormat:@"userid=%@&category=%d&subcategory=%d&type=%d",
                                  _userid.text,tokenCategoryV2,tokenSubcategoryV2,regCodeOnEmailV2
                                  ];
    NSLog(@"%@", parameterString);
    NSString* encodedUrl = [parameterString stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet];
    
    NSData *postData = [encodedUrl dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
    
    NSString *postLength=[NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSString* urlDetails = self.getURLV2;
    NSString* post_url= [NSString stringWithFormat:urlDetails,@"/ResendActivationCode"];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@",post_url]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString* aStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] ;
    NSLog(@"responseData %@",aStr);
    return aStr;
}

-(NSString*) resendActivationCodeOnSMSV2{
    
    NSCharacterSet * queryKVSet = [NSCharacterSet
                                   characterSetWithCharactersInString:@"+"
                                   ].invertedSet;
    
    NSString * parameterString = [NSString                                  stringWithFormat:@"userid=%@&category=%d&subcategory=%d&type=%d",
                                  _userid.text,tokenCategoryV2,tokenSubcategoryV2,regCodeOnSMSV2
                                  ];
    NSLog(@"%@", parameterString);
    NSString* encodedUrl = [parameterString stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet];
    
    NSData *postData = [encodedUrl dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
    
    NSString *postLength=[NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSString* urlDetails = self.getURLV2;
    NSString* post_url= [NSString stringWithFormat:urlDetails,@"/ResendActivationCode"];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@",post_url]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString* aStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] ;
    NSLog(@"responseData %@",aStr);
    return aStr;
}

-(BOOL) textFieldShouldReturn: (UITextField *) textField
{
    [textField resignFirstResponder];
    if(keyboardOpened!=nil && [keyboardOpened isEqualToString:@"YES"]){
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
        keyboardOpened=nil;
    }
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField==_mobileno){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        keyboardOpened=@"YES";
    }
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
}



#pragma mark - keyboard movements
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -keyboardSize.height;
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}
@end
