//
//  VerifyRegCodeController.h
//  AcledaPush
//
//  Created by Abhishek Ingle on 14/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerifyRegCodeController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *regCode;
- (IBAction)verifyRegCode:(id)sender;
- (IBAction)resentRegCodeOnEmail:(id)sender;
- (IBAction)resentRegCodeOnSMS:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *regCodeOne;
@property (weak, nonatomic) IBOutlet UITextField *regCodeTwo;
@property (weak, nonatomic) IBOutlet UITextField *regCodeThree;
@property (weak, nonatomic) IBOutlet UITextField *regCodeFour;
@property (weak, nonatomic) IBOutlet UITextField *regCodeFive;
@property (weak, nonatomic) IBOutlet UITextField *regCodeSix;
@property (weak, nonatomic) IBOutlet UITextField *regCodeSeven;
@property (weak, nonatomic) IBOutlet UITextField *regCodeEight;
@property ( nonatomic, strong ) NSString *userId;
@property (weak, nonatomic) IBOutlet UILabel *userIdLable;
@property (weak, nonatomic) IBOutlet UILabel *userPhoneLable;


@end
