//
//  SetMPINController.m
//  AcledaPush
//
//  Created by Abhishek Ingle on 16/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import "SetMPINController.h"
#import "LoginController.h"
#import "RegisterSuccessScreen.h"

@interface SetMPINController ()

@end

@implementation SetMPINController

static NSString *keyboardOpened=nil;
+(NSString*)keyboardOpened{return  keyboardOpened;}
+(void)setKeyboardOpened:(NSString*)value{
    keyboardOpened=value;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_setMPINOne becomeFirstResponder];
    
    [_setMPINOne addTarget:self action:@selector(setMPINOneChanged) forControlEvents:UIControlEventEditingChanged];
    [_setMPINTwo addTarget:self action:@selector(setMPINTwoChanged) forControlEvents:UIControlEventEditingChanged];
    [_setMPINThree addTarget:self action:@selector(setMPINThreeChanged) forControlEvents:UIControlEventEditingChanged];
    [_setMPINFour addTarget:self action:@selector(setMPINFourChanged) forControlEvents:UIControlEventEditingChanged];
    
    [_confirmSMPINOne addTarget:self action:@selector(setCMPINOneChanged) forControlEvents:UIControlEventEditingChanged];
    [_confirmSMPINTwo addTarget:self action:@selector(setCMPINTwoChanged) forControlEvents:UIControlEventEditingChanged];
    [_confirmSMPINThree addTarget:self action:@selector(setCMPINThreeChanged) forControlEvents:UIControlEventEditingChanged];
    [_confirmSMPINFour addTarget:self action:@selector(setCMPINFourChanged) forControlEvents:UIControlEventEditingChanged];
}

- (void)setMPINFourChanged{
    NSString* pinFour = _setMPINFour.text;
    if(![pinFour isEqualToString:@""]){
        _setMPINFour.secureTextEntry = YES;
        [self nextSetMPINFocus4];
    }
}
- (void)setMPINThreeChanged{
    NSString* pinThree = _setMPINThree.text;
    if(![pinThree isEqualToString:@""]){
        _setMPINThree.secureTextEntry = YES;
        [self nextSetMPINFocus3];
    }
}
- (void)setMPINTwoChanged{
    NSString* pinTwo = _setMPINTwo.text;
    if(![pinTwo isEqualToString:@""]){
        _setMPINTwo.secureTextEntry = YES;
        [self nextSetMPINFocus2];
    }
}
- (void)setMPINOneChanged{
    NSString* pinOne = _setMPINOne.text;
    if(![pinOne isEqualToString:@""]){
        _setMPINOne.secureTextEntry = YES;
        [self nextSetMPINFocus1];
    }
}

- (void)setCMPINFourChanged{
    NSString* pinFour = _confirmSMPINFour.text;
    if(![pinFour isEqualToString:@""]){
        _confirmSMPINFour.secureTextEntry = YES;
        [self nextCSetMPINFocus4];
    }
}
- (void)setCMPINThreeChanged{
    NSString* pinThree = _confirmSMPINThree.text;
    if(![pinThree isEqualToString:@""]){
        _confirmSMPINThree.secureTextEntry = YES;
        [self nextCSetMPINFocus3];
    }
}
- (void)setCMPINTwoChanged{
    NSString* pinTwo = _confirmSMPINTwo.text;
    if(![pinTwo isEqualToString:@""]){
        _confirmSMPINTwo.secureTextEntry = YES;
        [self nextCSetMPINFocus2];
    }
}
- (void)setCMPINOneChanged{
    NSString* pinOne = _confirmSMPINOne.text;
    if(![pinOne isEqualToString:@""]){
        _confirmSMPINOne.secureTextEntry = YES;
        [self nextCSetMPINFocus1];
    }
}

-(void) nextSetMPINFocus1 {
    [_setMPINTwo becomeFirstResponder];
}

-(void) nextSetMPINFocus2 {
    [_setMPINThree becomeFirstResponder];
}

-(void) nextSetMPINFocus3 {
    [_setMPINFour becomeFirstResponder];
}
-(void) nextSetMPINFocus4 {
    [_setMPINFour resignFirstResponder];
}

-(void) nextCSetMPINFocus1 {
    [_confirmSMPINTwo becomeFirstResponder];
}

-(void) nextCSetMPINFocus2 {
    [_confirmSMPINThree becomeFirstResponder];
}

-(void) nextCSetMPINFocus3 {
    [_confirmSMPINFour becomeFirstResponder];
}
-(void) nextCSetMPINFocus4 {
    [_confirmSMPINFour resignFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(BOOL) textFieldShouldReturn: (UITextField *) textField
{
    [textField resignFirstResponder];
    if(keyboardOpened!=nil && [keyboardOpened isEqualToString:@"YES"]){
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
        keyboardOpened=nil;
    }
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
//    if(textField== _confirmSMPINOne || textField== _confirmSMPINTwo || textField== _confirmSMPINThree || textField== _confirmSMPINFour){
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
//        keyboardOpened=@"YES";
//    }
    
    return YES;
}

// This allows numeric text only, but also backspace for deletes
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    // This 'tabs' to next field when entering digits
    if (newLength == 1) {
        if (textField == _setMPINOne)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_setMPINTwo afterDelay:0.2];
        }
        else if (textField == _setMPINTwo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_setMPINThree afterDelay:0.2];
        }
        else if (textField == _setMPINThree)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_setMPINFour afterDelay:0.2];
        }
        else if (textField == _confirmSMPINOne)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_confirmSMPINTwo afterDelay:0.2];
        }
        else if (textField == _confirmSMPINTwo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_confirmSMPINThree afterDelay:0.2];
        }
        else if (textField == _confirmSMPINThree)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_confirmSMPINFour afterDelay:0.2];
        }
    }
    //this goes to previous field as you backspace through them, so you don't have to tap into them individually
    else if (oldLength > 0 && newLength == 0) {
        if (textField == _setMPINFour)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_setMPINThree afterDelay:0.1];
        }
        else if (textField == _setMPINThree)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_setMPINTwo afterDelay:0.1];
        }
        else if (textField == _setMPINTwo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_setMPINOne afterDelay:0.1];
        }
        else if (textField == _confirmSMPINFour)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_confirmSMPINThree afterDelay:0.1];
        }
        else if (textField == _confirmSMPINThree)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_confirmSMPINTwo afterDelay:0.1];
        }
        else if (textField == _confirmSMPINTwo)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_confirmSMPINOne afterDelay:0.1];
        }
    }
    
    return newLength <= 1;
}

- (void)setNextResponder:(UITextField *)nextResponder
{
    [nextResponder becomeFirstResponder];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -keyboardSize.height;
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}
-(void)textFieldDidEndEditing:(UITextField *)textField {
    
}
UIAlertController *progressControllerForSetMPIN ;

-(Boolean) isInvalidInput{
    if([_setMPINOne.text isEqualToString:@""] || [_setMPINTwo.text isEqualToString:@""] || [_setMPINThree.text isEqualToString:@""] || [_setMPINFour.text isEqualToString:@""]){
        return true;
    }
    if([_confirmSMPINOne.text isEqualToString:@""] || [_confirmSMPINTwo.text isEqualToString:@""] || [_confirmSMPINThree.text isEqualToString:@""] || [_confirmSMPINFour.text isEqualToString:@""]){
        return true;
    }
    return false;
}

-(void) doSetMPIN{

    Boolean vaildInput = self.isInvalidInput;
    if(vaildInput){
        [self dismissViewControllerAnimated:progressControllerForSetMPIN completion:^{
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Your PIN must be of four digit" preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
        }];
    }
    NSString *valueToSave = [NSString stringWithFormat:@"%@%@%@%@",_setMPINOne.text,_setMPINTwo.text,_setMPINThree.text,_setMPINFour.text];
    
    NSString *valueToSaveConfirm = [NSString stringWithFormat:@"%@%@%@%@",_confirmSMPINOne.text,_confirmSMPINTwo.text,_confirmSMPINThree.text,_confirmSMPINFour.text];
    //NSLog(@"Set Mapin Called key %@",_userId);
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *currentUserRegister = [prefs objectForKey:@"currentRegisterInUser"];
    NSLog(@"currentUserReg for setPIN%@",currentUserRegister);
    if([valueToSave isEqualToString:valueToSaveConfirm]){
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setObject:valueToSave forKey:currentUserRegister];
        NSLog(@"userMPIN %@",[prefs stringForKey:currentUserRegister]);
        [prefs synchronize];
        [NSThread sleepForTimeInterval:2.0f];
        //    UIAlertController *failureAlertController =nil;
        //    failureAlertController = [UIAlertController alertControllerWithTitle:@"Success" message:@"MPIN Set Successfully" preferredStyle:UIAlertControllerStyleAlert];
        //    [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        //
        //    }]];
            [self dismissViewControllerAnimated:progressControllerForSetMPIN completion:^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    RegisterSuccessScreen *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"registerSuccessController"];
                    [self presentViewController:vc animated:YES completion:nil];
                });
            }];
        //[self presentViewController:failureAlertController animated:YES completion:nil];
    }else{
        [self dismissViewControllerAnimated:progressControllerForSetMPIN completion:^{
            UIAlertController *failureAlertController =nil;
            failureAlertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Your PIN does not match with Confirm MPIN" preferredStyle:UIAlertControllerStyleAlert];
            [failureAlertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:failureAlertController animated:YES completion:nil];
        }];
    }
}
- (IBAction)setMPIN:(id)sender {
    progressControllerForSetMPIN = [UIAlertController alertControllerWithTitle:@"Please wait" message:@"Set PIN is in process ..." preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:progressControllerForSetMPIN animated:YES completion:^{
        dispatch_async(dispatch_get_main_queue(), ^{[self doSetMPIN];});
    }];
}
@end
