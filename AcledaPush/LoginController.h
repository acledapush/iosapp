//
//  LoginController.h
//  AcledaPush
//
//  Created by Abhishek Ingle on 16/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *loginUserId;
@property (weak, nonatomic) IBOutlet UITextField *loginMPIN;
- (IBAction)loginUser:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *firstDigitOfMPIN;
@property (weak, nonatomic) IBOutlet UITextField *secondDigitOfMPIN;
@property (weak, nonatomic) IBOutlet UITextField *thirdDigitOfMPIN;

@property (weak, nonatomic) IBOutlet UITextField *fourthDigitOfMPIN;


@end
