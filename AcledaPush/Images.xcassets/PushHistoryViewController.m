//
//  PushHistoryViewController.m
//  AcledaPush
//
//  Created by Abhishek Ingle on 29/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import "PushHistoryViewController.h"
#import "SWRevealViewController.h"
#import "TransactionDetails.h"

@interface PushHistoryViewController ()<UITableViewDataSource, UITableViewDelegate>{
    NSMutableArray *pushTransactionHistoryDetails;
    NSMutableArray *pushTransactionHistoryDetailsTwo;
    NSMutableArray *pushTransactionHistoryDetailsThree;
    NSMutableArray *pushTransactionHistoryDetailsFour;
    NSMutableArray *pushTransactionHistoryDetailsFive;
    NSMutableArray *pushTransactionHistoryDetailsNone;
}

@property (weak, nonatomic) IBOutlet UITableView *pushHistoryTable;

@end

@implementation PushHistoryViewController
BOOL isLoaded = false;
NSInteger* howManyTxPresent=0;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self pushHistoryArray];
    [self pushHistoryArrayTwo];
    [self pushHistoryArrayThree];
    [self pushHistoryArrayFour];
    [self pushHistoryArrayFive];
    [self pushHistoryArrayNone];
    isLoaded = true;
    howManyTxPresent = [self howManyObjPresent];
    //NSLog(@"How many TX present %d >> ",howManyTxPresent);
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *pushHistoryUserId = [prefs objectForKey:@"currentLogiedInUser"];
    _pushHistoryUserId.text = pushHistoryUserId;
    self.pushHistoryTable.separatorColor = [UIColor clearColor];
    self.pushHistoryTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    // Do any additional setup after loading the view.
    _barButton.target = self.revealViewController;
    _barButton.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//
- (void)pushHistoryArray {
    pushTransactionHistoryDetails = [NSMutableArray arrayWithArray:@[@"Action",@"Status",@"IP",@"Location",@"Date",@"Expiry Time",@"Message"]];
}
- (void)pushHistoryArrayTwo {
    pushTransactionHistoryDetailsTwo = [NSMutableArray arrayWithArray:@[@"Action",@"Status",@"IP",@"Location",@"Date",@"Expiry Time",@"Message",@"",
        @"Action",@"Status",@"IP",@"Location",@"Date",@"Expiry Time",@"Message"]];
}
- (void)pushHistoryArrayThree {
    pushTransactionHistoryDetailsThree = [NSMutableArray arrayWithArray:@[@"Action",@"Status",@"IP",@"Location",@"Date",@"Expiry Time",@"Message",@"",
        @"Action",@"Status",@"IP",@"Location",@"Date",@"Expiry Time",@"Message",@"",@"Action",@"Status",@"IP",@"Location",@"Date",@"Expiry Time",@"Message"]];
}
- (void)pushHistoryArrayFour {
    pushTransactionHistoryDetailsFour = [NSMutableArray arrayWithArray:@[@"Action",@"Status",@"IP",@"Location",@"Date",@"Expiry Time",@"Message",
        @"",@"Action",@"Status",@"IP",@"Location",@"Date",@"Expiry Time",@"Message",
        @"",@"Action",@"Status",@"IP",@"Location",@"Date",@"Expiry Time",@"Message",
        @"",@"Action",@"Status",@"IP",@"Location",@"Date",@"Expiry Time",@"Message"]];
}
- (void)pushHistoryArrayFive {
    pushTransactionHistoryDetailsFive = [NSMutableArray arrayWithArray:@[@"Action",@"IP",@"Location",@"Date",@"Expiry Time",@"Message",@"Status",
        @"",@"Action",@"IP",@"Location",@"Date",@"Expiry Time",@"Message",@"Status",
        @"",@"Action",@"IP",@"Location",@"Date",@"Expiry Time",@"Message",@"Status",
        @"",@"Action",@"IP",@"Location",@"Date",@"Expiry Time",@"Message",@"Status",
        @"",@"Action",@"IP",@"Location",@"Date",@"Expiry Time",@"Message",@"Status"]];
}
- (void)pushHistoryArrayNone {
    pushTransactionHistoryDetailsNone = [NSMutableArray arrayWithArray:@[@" You dont have any push notification yet"
        ]];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(howManyTxPresent == 1){
        return pushTransactionHistoryDetails.count;
    }else if(howManyTxPresent == 2){
        return pushTransactionHistoryDetailsTwo.count;
    }else if(howManyTxPresent == 3){
        return pushTransactionHistoryDetailsThree.count;
    }else if(howManyTxPresent == 4){
        return pushTransactionHistoryDetailsFour.count;
    }else if(howManyTxPresent == 5){
        return pushTransactionHistoryDetailsFive.count;
    }else{
        return pushTransactionHistoryDetailsNone.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellId = @"historyCell";
    
    TransactionDetails *txObj = [self returnPushData];
    isLoaded = false;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if(howManyTxPresent == 1){
        cell.textLabel.text = pushTransactionHistoryDetails[indexPath.row];
    }else if(howManyTxPresent == 2){
        cell.textLabel.text = pushTransactionHistoryDetailsTwo[indexPath.row];
    }else if(howManyTxPresent == 3){
        cell.textLabel.text = pushTransactionHistoryDetailsThree[indexPath.row];
    }else if(howManyTxPresent == 4){
        cell.textLabel.text = pushTransactionHistoryDetailsFour[indexPath.row];
    }else if(howManyTxPresent == 5){
        cell.textLabel.text = pushTransactionHistoryDetailsFive[indexPath.row];
    }else if(howManyTxPresent == 0){
        cell.textLabel.text = pushTransactionHistoryDetailsNone[indexPath.row];
    }
    int txRow = (int)indexPath.row + 1;
    //txId=txObj.transactionID;
    
//    if(txRow == 1){
//        if(howManyTxPresent == 0){
//            cell.detailTextLabel.text = @" ";
//        }else{
//            cell.detailTextLabel.text = txObj.transactionID;
//        }
//    }else
    if(txRow == 1){
        if([txObj.transactionAction isEqualToString:@"T"]){
            cell.detailTextLabel.text = @"Transaction";
        }else if([txObj.transactionAction isEqualToString:@"L"]){
            cell.detailTextLabel.text = @"Login";
        }
        else{
            cell.detailTextLabel.text = @"   ";
        }
    }
    else if(txRow == 2){
        if([txObj.transactionStatus isEqualToString:@"0"]){
            cell.detailTextLabel.text = @"Pending";
        }else if([txObj.transactionStatus isEqualToString:@"1"]){
            cell.detailTextLabel.text = @"Approved";
        }else if([txObj.transactionStatus isEqualToString:@"2"]){
            cell.detailTextLabel.text = @"Denied";
        }else if([txObj.transactionStatus isEqualToString:@"-1"]){
            cell.detailTextLabel.text = @"Expired";
        }
    }else if(txRow == 3){
        cell.detailTextLabel.text = txObj.transactionIp;
        
    }else if(txRow == 4){
        cell.detailTextLabel.text = txObj.transactionLocation;
        
    }else if(txRow == 5){
        cell.detailTextLabel.text = txObj.transactionDate;
        
    }else if(txRow == 6){
        cell.detailTextLabel.text = txObj.transactionExpiryTime;
    }else if(txRow == 7){
        cell.detailTextLabel.text = txObj.transactionMessage;
    }else if(txRow ==8){
        cell.detailTextLabel.text = @" ";
    }
    else if(txRow == 9){
        if([txObj.transactionActionTwo isEqualToString:@"T"]){
            cell.detailTextLabel.text = @"Transaction";
        }else if([txObj.transactionActionTwo isEqualToString:@"L"]){
            cell.detailTextLabel.text = @"Login";
        }else{
            cell.detailTextLabel.text = @"NA";
        }
    }
    else if(txRow == 10){
        if([txObj.transactionStatusTwo isEqualToString:@"0"]){
            cell.detailTextLabel.text = @"Pending";
        }else if([txObj.transactionStatusTwo isEqualToString:@"1"]){
            cell.detailTextLabel.text = @"Approved";
        }else if([txObj.transactionStatusTwo isEqualToString:@"2"]){
            cell.detailTextLabel.text = @"Denied";
        }else if([txObj.transactionStatusTwo isEqualToString:@"-1"]){
            cell.detailTextLabel.text = @"Expired";
        }
        
    }else if(txRow == 11){
        cell.detailTextLabel.text = txObj.transactionIpTwo;
    }else if(txRow == 12){
        cell.detailTextLabel.text = txObj.transactionLocationTwo;
    }else if(txRow == 13){
        cell.detailTextLabel.text = txObj.transactionDateTwo;
    }else if(txRow == 14){
        cell.detailTextLabel.text = txObj.transactionExpiryTimeTwo;
    }
    else if(txRow == 15){
        cell.detailTextLabel.text = txObj.transactionMessageTwo;
    }else if(txRow ==16){
        cell.detailTextLabel.text = @" ";
    }else if(txRow == 17){
        if([txObj.transactionActionThree isEqualToString:@"T"]){
            cell.detailTextLabel.text = @"Transaction";
        }else if([txObj.transactionActionThree isEqualToString:@"L"]){
            cell.detailTextLabel.text = @"Login";
        }else{
            cell.detailTextLabel.text = @"NA";
        }
    }
    else if(txRow == 18){
        if([txObj.transactionStatusThree isEqualToString:@"0"]){
            cell.detailTextLabel.text = @"Pending";
        }else if([txObj.transactionStatusThree isEqualToString:@"1"]){
            cell.detailTextLabel.text = @"Approved";
        }else if([txObj.transactionStatusThree isEqualToString:@"2"]){
            cell.detailTextLabel.text = @"Denied";
        }else if([txObj.transactionStatusThree isEqualToString:@"-1"]){
            cell.detailTextLabel.text = @"Expired";
        }
        
    }else if(txRow == 19){
        cell.detailTextLabel.text = txObj.transactionIpThree;
    }else if(txRow == 20){
        cell.detailTextLabel.text = txObj.transactionLocationThree;
    }else if(txRow == 21){
        cell.detailTextLabel.text = txObj.transactionDateThree;
    }else if(txRow == 22){
        cell.detailTextLabel.text = txObj.transactionExpiryTimeThree;
    }else if(txRow == 23){
        cell.detailTextLabel.text = txObj.transactionMessageThree;
    }else if(txRow ==24){
        cell.detailTextLabel.text = @" ";
    }else if(txRow == 25){
        if([txObj.transactionActionFour isEqualToString:@"T"]){
            cell.detailTextLabel.text = @"Transaction";
        }else if([txObj.transactionActionFour isEqualToString:@"L"]){
            cell.detailTextLabel.text = @"Login";
        }else{
            cell.detailTextLabel.text = @"NA";
        }
    }
    else if(txRow == 26){
        if([txObj.transactionStatusFour isEqualToString:@"0"]){
            cell.detailTextLabel.text = @"Pending";
        }else if([txObj.transactionStatusFour isEqualToString:@"1"]){
            cell.detailTextLabel.text = @"Approved";
        }else if([txObj.transactionStatusFour isEqualToString:@"2"]){
            cell.detailTextLabel.text = @"Denied";
        }else if([txObj.transactionStatusFour isEqualToString:@"-1"]){
            cell.detailTextLabel.text = @"Expired";
        }
    }else if(txRow == 27){
        cell.detailTextLabel.text = txObj.transactionIpFour;
    }else if(txRow == 28){
        cell.detailTextLabel.text = txObj.transactionLocationFour;
    }else if(txRow == 29){
        cell.detailTextLabel.text = txObj.transactionDateFour;
    }else if(txRow == 30){
        cell.detailTextLabel.text = txObj.transactionExpiryTimeFour;
    }else if(txRow == 31){
        cell.detailTextLabel.text = txObj.transactionMessageFour;
    }else if(txRow ==32){
        cell.detailTextLabel.text = @" ";
    }else if(txRow == 33){
        if([txObj.transactionActionFour isEqualToString:@"T"]){
            cell.detailTextLabel.text = @"Transaction";
        }else if([txObj.transactionActionFour isEqualToString:@"L"]){
            cell.detailTextLabel.text = @"Login";
        }else{
            cell.detailTextLabel.text = @"NA";
        }
    }
    else if(txRow == 34){
        if([txObj.transactionStatusFive isEqualToString:@"0"]){
            cell.detailTextLabel.text = @"Pending";
        }else if([txObj.transactionStatusFive isEqualToString:@"1"]){
            cell.detailTextLabel.text = @"Approved";
        }else if([txObj.transactionStatusFive isEqualToString:@"2"]){
            cell.detailTextLabel.text = @"Denied";
        }else if([txObj.transactionStatusFive isEqualToString:@"-1"]){
            cell.detailTextLabel.text = @"Expired";
        }
        
    }else if(txRow == 35){
        cell.detailTextLabel.text = txObj.transactionIpFive;
    }else if(txRow == 36){
        cell.detailTextLabel.text = txObj.transactionLocationFive;
    }else if(txRow == 37){
        cell.detailTextLabel.text = txObj.transactionDateFive;
    }else if(txRow == 38){
        cell.detailTextLabel.text = txObj.transactionExpiryTimeFive;
    }else if(txRow == 39){
        cell.detailTextLabel.text = txObj.transactionMessageFive;
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}
-(NSString *) LoadFile:(NSString *)filename{
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *strFileName = [NSString stringWithFormat:@"%@/%@",
                             documentsDirectory,filename];
    NSString *content = [[NSString alloc] initWithContentsOfFile:strFileName
                                                    usedEncoding:nil
                                                           error:nil];
    return content;
    //use simple alert from my library (see previous post for details)
    // [ASFunctions alert:content];
    //  [content release];
    
}

-(NSInteger *) howManyObjPresent{
    NSInteger *objPresent;
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *currentUserLogin = [prefs objectForKey:@"currentLogiedInUser"];
    NSLog(@"user returnPushData >> %@",currentUserLogin);
    NSString *strPreviousJsonEncData = [self LoadFile:[@"historyPushDetails" stringByAppendingString:currentUserLogin]];
    if(strPreviousJsonEncData != nil){
        NSData *data = [strPreviousJsonEncData dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSString *TxOnePushHistory = [json objectForKey:@"TxOne"];
        NSString *TxTwoPushHistory = [json objectForKey:@"TxTwo"];
        NSString *TxThreePushHistory = [json objectForKey:@"TxThree"];
        NSString *TxFourPushHistory = [json objectForKey:@"TxFour"];
        NSString *TxFivePushHistory = [json objectForKey:@"TxFive"];
        if(TxFivePushHistory != nil){
            objPresent = 5;
        }else if(TxFourPushHistory != nil){
            objPresent = 4;
        }else if(TxThreePushHistory != nil){
            objPresent = 3;
        }else if (TxTwoPushHistory != nil){
            objPresent = 2;
        }else if (TxOnePushHistory != nil){
            objPresent = 1;
        }
        return objPresent;
    }else{
        objPresent = 0;
    }
    return objPresent;
}

-(TransactionDetails *) returnPushData{
    NSString* txIP = @"NA";
    NSString* txAction = @"NA";
    NSString* txMessage = @"NA";
    NSString* txDate = @"NA";
    NSString* txExpiry = @"NA";
    NSString* txID = @"NA";
    NSString* txStatus = @"9";
    NSString* txLocation =@"NA";
    
    NSString* txIPTwo = @"NA";
    NSString* txActionTwo = @"NA";
    NSString* txMessageTwo = @"NA";
    NSString* txDateTwo = @"NA";
    NSString* txExpiryTwo = @"NA";
    NSString* txIDTwo = @"NA";
    NSString* txStatusTwo = @"9";
    NSString* txLocationTwo =@"NA";
    
    NSString* txIPThree = @"NA";
    NSString* txActionThree = @"NA";
    NSString* txMessageThree = @"NA";
    NSString* txDateThree = @"NA";
    NSString* txExpiryThree = @"NA";
    NSString* txIDThree = @"NA";
    NSString* txStatusThree = @"9";
    NSString* txLocationThree =@"NA";
    
    NSString* txIPFour = @"NA";
    NSString* txActionFour = @"NA";
    NSString* txMessageFour = @"NA";
    NSString* txDateFour = @"NA";
    NSString* txExpiryFour = @"NA";
    NSString* txIDFour = @"NA";
    NSString* txStatusFour = @"9";
    NSString* txLocationFour =@"NA";
    
    NSString* txIPFive = @"NA";
    NSString* txActionFive = @"NA";
    NSString* txMessageFive = @"NA";
    NSString* txDateFive = @"NA";
    NSString* txExpiryFive = @"NA";
    NSString* txIDFive = @"NA";
    NSString* txStatusFive = @"9";
    NSString* txLocationFive =@"NA";
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *currentUserLogin = [prefs objectForKey:@"currentLogiedInUser"];
    TransactionDetails *transactionDetails = [[TransactionDetails alloc]init];
    NSString *strPreviousJsonEncData = [self LoadFile:[@"historyPushDetails" stringByAppendingString:currentUserLogin]];
    NSLog(@"TxFileReadDate %@",strPreviousJsonEncData);
    
    if(strPreviousJsonEncData != nil){
        NSData *data = [strPreviousJsonEncData dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        //NSLog(@"TxOne %@", [json objectForKey:@"TxOne"]);
        NSString *TxOnePushHistory = [json objectForKey:@"TxOne"];
        NSString *TxTwoPushHistory = [json objectForKey:@"TxTwo"];
        NSString *TxThreePushHistory = [json objectForKey:@"TxThree"];
        NSString *TxFourPushHistory = [json objectForKey:@"TxFour"];
        NSString *TxFivePushHistory = [json objectForKey:@"TxFive"];
       // NSString* status = [json objectForKey:@"status"];
        
        if(TxOnePushHistory!= nil){
            NSData *pushdata = [TxOnePushHistory dataUsingEncoding:NSUTF8StringEncoding];
            id pushDatajson = [NSJSONSerialization JSONObjectWithData:pushdata options:0 error:nil];
            NSLog(@"pushDatajson %@", pushDatajson);
            NSString *TxOnePushData = [pushDatajson objectForKey:@"pushData"];
            NSString *TxOneStatus = [pushDatajson objectForKey:@"status"];
            NSData *txOnePushData = [TxOnePushData dataUsingEncoding:NSUTF8StringEncoding];
            id txOnePushDatajson = [NSJSONSerialization JSONObjectWithData:txOnePushData options:0 error:nil];
            
            txIP = [txOnePushDatajson objectForKey:@"IP"];
            txAction = [txOnePushDatajson objectForKey:@"Action"];
            txDate = [txOnePushDatajson objectForKey:@"Date"];
            txMessage = [txOnePushDatajson objectForKey:@"Message"];
            txExpiry = [txOnePushDatajson objectForKey:@"Expiry Time"];
            txID = [txOnePushDatajson objectForKey:@"pushTxId"];
            txStatus = TxOneStatus;
            txLocation = [txOnePushDatajson objectForKey:@"Location"];
        }
        // set transaction details
        [transactionDetails setTransactionIp:txIP];
        [transactionDetails setTransactionDate:txDate];
        [transactionDetails setTransactionMessage:txMessage];
        [transactionDetails setTransactionExpiryTime:txExpiry];
        [transactionDetails setTransactionID:txID];
        [transactionDetails setTransactionAction:txAction];
        [transactionDetails setTransactionStatus:txStatus];
        [transactionDetails setTransactionLocation:txLocation];
        // second tx details
        if(TxTwoPushHistory!= nil){
            NSData *pushdata = [TxTwoPushHistory dataUsingEncoding:NSUTF8StringEncoding];
            id pushDatajson = [NSJSONSerialization JSONObjectWithData:pushdata options:0 error:nil];
            NSLog(@"pushDatajson %@", pushDatajson);
            NSString *TxOnePushData = [pushDatajson objectForKey:@"pushData"];
            NSString *TxOneStatus = [pushDatajson objectForKey:@"status"];
            NSData *txOnePushData = [TxOnePushData dataUsingEncoding:NSUTF8StringEncoding];
            id txOnePushDatajson = [NSJSONSerialization JSONObjectWithData:txOnePushData options:0 error:nil];
            
            txIPTwo = [txOnePushDatajson objectForKey:@"IP"];
            txActionTwo = [txOnePushDatajson objectForKey:@"Action"];
            txDateTwo = [txOnePushDatajson objectForKey:@"Date"];
            txMessageTwo = [txOnePushDatajson objectForKey:@"Message"];
            txExpiryTwo = [txOnePushDatajson objectForKey:@"Expiry Time"];
            txIDTwo = [txOnePushDatajson objectForKey:@"pushTxId"];
            txStatusTwo = TxOneStatus;
            txLocationTwo = [txOnePushDatajson objectForKey:@"Location"];
        }
            // set transaction details
            [transactionDetails setTransactionIpTwo:txIPTwo];
            [transactionDetails setTransactionDateTwo:txDateTwo];
            [transactionDetails setTransactionMessageTwo:txMessageTwo];
            [transactionDetails setTransactionExpiryTimeTwo:txExpiryTwo];
            [transactionDetails setTransactionIDTwo:txIDTwo];
            [transactionDetails setTransactionActionTwo:txActionTwo];
            [transactionDetails setTransactionStatusTwo:txStatusTwo];
            [transactionDetails setTransactionLocationTwo:txLocationTwo];

        
        // third tx details
        if(TxThreePushHistory!= nil){
            NSData *pushdata = [TxThreePushHistory dataUsingEncoding:NSUTF8StringEncoding];
            id pushDatajson = [NSJSONSerialization JSONObjectWithData:pushdata options:0 error:nil];
            NSLog(@"pushDatajson %@", pushDatajson);
            NSString *TxOnePushData = [pushDatajson objectForKey:@"pushData"];
            NSString *TxOneStatus = [pushDatajson objectForKey:@"status"];
            NSData *txOnePushData = [TxOnePushData dataUsingEncoding:NSUTF8StringEncoding];
            id txOnePushDatajson = [NSJSONSerialization JSONObjectWithData:txOnePushData options:0 error:nil];
            
            txIPThree = [txOnePushDatajson objectForKey:@"IP"];
            txActionThree = [txOnePushDatajson objectForKey:@"Action"];
            txDateThree = [txOnePushDatajson objectForKey:@"Date"];
            txMessageThree = [txOnePushDatajson objectForKey:@"Message"];
            txExpiryThree = [txOnePushDatajson objectForKey:@"Expiry Time"];
            txIDThree = [txOnePushDatajson objectForKey:@"pushTxId"];
            txStatusThree = TxOneStatus;
            txLocationThree = [txOnePushDatajson objectForKey:@"Location"];
        }
        // set transaction details
        [transactionDetails setTransactionIpThree:txIPThree];
        [transactionDetails setTransactionDateThree:txDateThree];
        [transactionDetails setTransactionMessageThree:txMessageThree];
        [transactionDetails setTransactionExpiryTimeThree:txExpiryThree];
        [transactionDetails setTransactionIDThree:txIDThree];
        [transactionDetails setTransactionActionThree:txActionThree];
        [transactionDetails setTransactionStatusThree:txStatusThree];
        [transactionDetails setTransactionLocationThree:txLocationThree];
        // four tx details
        if(TxFourPushHistory!= nil){
            NSData *pushdata = [TxFourPushHistory dataUsingEncoding:NSUTF8StringEncoding];
            id pushDatajson = [NSJSONSerialization JSONObjectWithData:pushdata options:0 error:nil];
            NSLog(@"pushDatajson %@", pushDatajson);
            NSString *TxOnePushData = [pushDatajson objectForKey:@"pushData"];
            NSString *TxOneStatus = [pushDatajson objectForKey:@"status"];
            NSData *txOnePushData = [TxOnePushData dataUsingEncoding:NSUTF8StringEncoding];
            id txOnePushDatajson = [NSJSONSerialization JSONObjectWithData:txOnePushData options:0 error:nil];
            
            txIPFour = [txOnePushDatajson objectForKey:@"IP"];
            txActionFour = [txOnePushDatajson objectForKey:@"Action"];
            txDateFour = [txOnePushDatajson objectForKey:@"Date"];
            txMessageFour = [txOnePushDatajson objectForKey:@"Message"];
            txExpiryFour = [txOnePushDatajson objectForKey:@"Expiry Time"];
            txIDFour = [txOnePushDatajson objectForKey:@"pushTxId"];
            txStatusFour = TxOneStatus;
            txLocationFour = [txOnePushDatajson objectForKey:@"Location"];
            //
        }
        // set transaction details
        [transactionDetails setTransactionIpFour:txIPFour];
        [transactionDetails setTransactionDateFour:txDateFour];
        [transactionDetails setTransactionMessageFour:txMessageFour];
        [transactionDetails setTransactionExpiryTimeFour:txExpiryFour];
        [transactionDetails setTransactionIDFour:txIDFour];
        [transactionDetails setTransactionActionFour:txActionFour];
        [transactionDetails setTransactionStatusFour:txStatusFour];
        [transactionDetails setTransactionLocationFour:txLocationFour];
        // five tx details
        if(TxFivePushHistory!= nil){
            NSData *pushdata = [TxFivePushHistory dataUsingEncoding:NSUTF8StringEncoding];
            id pushDatajson = [NSJSONSerialization JSONObjectWithData:pushdata options:0 error:nil];
            NSLog(@"pushDatajson %@", pushDatajson);
            NSString *TxOnePushData = [pushDatajson objectForKey:@"pushData"];
            NSString *TxOneStatus = [pushDatajson objectForKey:@"status"];
            NSData *txOnePushData = [TxOnePushData dataUsingEncoding:NSUTF8StringEncoding];
            id txOnePushDatajson = [NSJSONSerialization JSONObjectWithData:txOnePushData options:0 error:nil];
            
            txIPFive = [txOnePushDatajson objectForKey:@"IP"];
            txActionFive = [txOnePushDatajson objectForKey:@"Action"];
            txDateFive = [txOnePushDatajson objectForKey:@"Date"];
            txMessageFive = [txOnePushDatajson objectForKey:@"Message"];
            txExpiryFive = [txOnePushDatajson objectForKey:@"Expiry Time"];
            txIDFive = [txOnePushDatajson objectForKey:@"pushTxId"];
            txStatusFive = TxOneStatus;
            txLocationFive = [txOnePushDatajson objectForKey:@"Location"];
        }
        // set transaction details
        [transactionDetails setTransactionIpFive:txIPFive];
        [transactionDetails setTransactionDateFive:txDateFive];
        [transactionDetails setTransactionMessageFive:txMessageFive];
        [transactionDetails setTransactionExpiryTimeFive:txExpiryFive];
        [transactionDetails setTransactionIDFive:txIDFive];
        [transactionDetails setTransactionActionFive:txActionFive];
        [transactionDetails setTransactionStatusFive:txStatusFive];
        [transactionDetails setTransactionLocationFive:txLocationFive];
    }else{
        [transactionDetails setTransactionIp:txIP];
        [transactionDetails setTransactionDate:txDate];
        [transactionDetails setTransactionMessage:txMessage];
        [transactionDetails setTransactionExpiryTime:txExpiry];
        [transactionDetails setTransactionID:txID];
        [transactionDetails setTransactionAction:txAction];
        [transactionDetails setTransactionStatus:txStatus];
        [transactionDetails setTransactionLocation:txLocation];
    }
    return transactionDetails;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
