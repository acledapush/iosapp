//
//  TransactionViewController.m
//  AcledaPush
//
//  Created by Abhishek Ingle on 22/03/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import "TransactionViewController.h"
#import "TransactionDetails.h"

@interface TransactionViewController ()<UITableViewDataSource,UITableViewDelegate>{
  NSMutableArray *pushTransactionDetails;
    NSArray *titles;
    NSArray *description;
}
@property (weak, nonatomic) IBOutlet UITableView *table;

@end

@implementation TransactionViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self arraySetUp];
    self.title=@"Acleda Push Auth";
    titles = [[NSArray alloc]initWithObjects:@"Action",@"IP",@"Date",@"Expiry Time",@"Message", nil];
    TransactionDetails *transactionDetails = self.returnPushData;
    description = [[NSArray alloc]initWithObjects:@"Transaction",transactionDetails.transactionIp,transactionDetails.transactionDate,transactionDetails.transactionExpiryTime,transactionDetails.transactionMessage, nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)arraySetUp {
    pushTransactionDetails = [NSMutableArray arrayWithArray:@[@"Action",@"IP",@"Date",@"Expiry Time",@"Message"]];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return titles.count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    static NSString *cellId = @"cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
//    // Read Push Data
//    TransactionDetails *transactionDetails = self.returnPushData;
//    cell.textLabel.text = pushTransactionDetails[indexPath.row];
//    int integerRow = (int)indexPath.row+1;
//    if(integerRow == 1){
//        cell.detailTextLabel.text = @"Transaction";
//    }else if(integerRow == 2){
//        cell.detailTextLabel.text = transactionDetails.transactionIp;
//    }else if(integerRow == 3){
//        cell.detailTextLabel.text = transactionDetails.transactionDate;
//    }else if(integerRow == 4){
//        cell.detailTextLabel.text = transactionDetails.transactionExpiryTime;
//    }else if (integerRow == 5){
//
//        cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
//        cell.textLabel.numberOfLines = 0;
//        [cell.textLabel sizeToFit];
//        cell.detailTextLabel.text = transactionDetails.transactionMessage;
//    }
//
//    return cell;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"transactionCell" forIndexPath:indexPath];
    [self updateCellWithTitle:[titles objectAtIndex:indexPath.row] description:[description objectAtIndex:indexPath.row]];
    return cell;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    CGSize labelSize = CGSizeMake(200.0, 20.0);
//    TransactionDetails *transactionDetails = self.returnPushData;
//    NSString *strTemp = transactionDetails.transactionMessage;
//
//    if ([strTemp length] > 0)
//        labelSize = [strTemp sizeWithFont: [UIFont boldSystemFontOfSize: 14.0] constrainedToSize: CGSizeMake(labelSize.width, 1000) lineBreakMode: UILineBreakModeWordWrap];
//
//    return (labelSize.height + 10);
//}
-(NSString *) LoadFile:(NSString *)filename{
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *strFileName = [NSString stringWithFormat:@"%@/%@",
                             documentsDirectory,filename];
    NSString *content = [[NSString alloc] initWithContentsOfFile:strFileName
                                                    usedEncoding:nil
                                                           error:nil];
    return content;
    //use simple alert from my library (see previous post for details)
    // [ASFunctions alert:content];
    //  [content release];
    
}

-(TransactionDetails *) returnPushData{
    NSString* txIP = @"NA";
    NSString* txAction = @"NA";
    NSString* txMessage = @"NA";
    NSString* txDate = @"NA";
    NSString* txExpiry = @"NA";
    TransactionDetails *transactionDetails = [[TransactionDetails alloc]init];
    NSString *strPreviousJsonEncData = [self LoadFile:[@"livePushDetails" stringByAppendingString:@"abhishekTest2"]];
    NSLog(@"previousJSON %@",strPreviousJsonEncData);
    
    if(strPreviousJsonEncData != nil){
        NSData *data = [strPreviousJsonEncData dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"pushData %@", [json objectForKey:@"pushData"]);
        NSString *strPushData = [json objectForKey:@"pushData"];
        NSData *pushdata = [strPushData dataUsingEncoding:NSUTF8StringEncoding];
        id pushDatajson = [NSJSONSerialization JSONObjectWithData:pushdata options:0 error:nil];
        NSLog(@"pushDatajson %@", pushDatajson);
        
        txIP = [pushDatajson objectForKey:@"IP"];
        txAction = @"Transaction";
        txDate = [pushDatajson objectForKey:@"Date"];
        txMessage = [pushDatajson objectForKey:@"message"];
        txExpiry = [pushDatajson objectForKey:@"ExpiryTime"];
        
        // set transaction details
        [transactionDetails setTransactionIp:txIP];
        [transactionDetails setTransactionDate:txDate];
        [transactionDetails setTransactionMessage:txMessage];
        [transactionDetails setTransactionExpiryTime:txExpiry];
    }else{
        [transactionDetails setTransactionIp:txIP];
        [transactionDetails setTransactionDate:txDate];
        [transactionDetails setTransactionMessage:txMessage];
        [transactionDetails setTransactionExpiryTime:txExpiry];
    }
    return transactionDetails;
}

- (void) updateCellWithTitle:(NSString *)title description:(NSString *)desc{
    self.descLbl.text = desc;
    self.titleLbl.text = title;
}


@end
