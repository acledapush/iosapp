//
//  FirstTourScreenViewController.h
//  AcledaPush
//
//  Created by Abhishek Ingle on 02/04/18.
//  Copyright © 2018 bluebricks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstTourScreenViewController : UIViewController
- (IBAction)firstTourNext:(id)sender;
- (IBAction)firstTourSkip:(id)sender;

@end
